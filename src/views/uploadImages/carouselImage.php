<!-- Match List Starts -->
<div class="row top_tiles" style="margin: 10px 0;">
  <div class="col-md-6 col-sm-6 col-xs-12 tile">
     <h2>Add Carousel Images</h2>
  </div>
</div>
<div class="row" style="margin: 10px 0;">
  <div class="col-md-4 col-sm-4 col-xs-4 tile pull-right">
     <a href="javascript:void(0);" data-type="match" data-toggle="modal" data-target=".add_image" class="btn btn-primary accordionIcon">
     	<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>&nbsp;&nbsp;Add Image
     </a>
  </div>
</div>
<!-- Match List End -->

<div class="row">
<div class="col-md-12 col-sm-12 col-xs-12">
 <div class="x_panel">
    <div class="table-responsive">
       <table id="live_table" class="table table-bordered">
          <thead>
             <tr>
                <th>#</th>
                <th>Image</th>
                <th>URL</th>
                <th>Landing page</th>
                <th>display order</th>
                <th>Action</th>
             </tr>
          </thead>
          <tbody>
          	<?php foreach ($this->object['data'] as $key => $imageDetails) { 
          		
          	?>
             <tr>
                <td><?php echo $key; ?></td>
                <td><img src="<?php echo $imageDetails['imageUri']; ?>"/></td>
                <td><?php echo $imageDetails['imageUri']; ?></td>
                <td><?php echo $imageDetails['landingUrl']; ?></td>
                <td><?php echo $imageDetails['displayOrder']; ?></td>
                <td>
                    <button type="submit" class="remove-carousel btn btn-primary btn-xs" value= <?php echo $imageDetails['id'] ?>>Remove</button>
                </td>
             </tr>
  			<?php } ?>
          </tbody>
       </table>
    </div>
 </div>
</div>
</div>


<div class="modal fade add_image" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-lg">
<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
    </div>
    <div class="modal-body">
    <form type="POST" id="carouselUpload">
      <table  class="table add_image_container table-bordered">
      <tbody>
          <tr>
              <td>
                <div class="form-inline">
                    <label>Landing Page:</label>
                </div>          
              </td>
               <td>
                <div class="form-inline">
					<input class="form-control" name="landingUrl" placeholder="" value="">
                </div>          
              </td>
           </tr>
           <tr>
              <td>
                <div class="form-inline">
                    <label>Display Order:</label>
                </div>
              </td>
              <td>
                <div class="form-inline">
                    <input class="form-control" name="displayOrder" placeholder="" value="">
                </div>
              </td>
           </tr>
           <tr>
              <td>
                <div class="form-inline">
                    <label>File:</label>
                </div>
              </td>
              <td>
                <div class="form-inline">
                    <input class="form-control" type="file" name="imageToUpload" placeholder="" value="">
                </div>
              </td>
            </tr>
            <tr>
              <td class="text-center" colspan="2"><button type="submit" class="btn btn-primary">Save</button></td>

          </tr>
      </tbody>
  </table>
  </form>
    </div>
</div>
</div>

</div>