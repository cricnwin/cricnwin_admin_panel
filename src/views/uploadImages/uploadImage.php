<?php 

?>

<!-- Match List Starts -->
<div class="row top_tiles" style="margin: 10px 0;">
  <div class="col-md-6 col-sm-6 col-xs-12 tile">
     <h2>Match List</h2>
  </div>
  <div class="col-md-3 col-sm-3 col-xs-6 tile">
     <span>Live Match:</span>
     <h2><?php echo count($this->liveMatchList); ?></h2>
  </div>
  <div class="col-md-3 col-sm-3 col-xs-6 tile">
     <span>Upcoming Match:</span>
     <h2><?php echo count($this->upcomingMatchList); ?></h2>
  </div>
</div>
<!-- Match List End -->

<div class="clearfix"></div>

<!-- Live Match List Starts -->
<?php if(count($this->object['data']) > 0) { ?>
<div class="row">
<div class="col-md-12 col-sm-12 col-xs-12">
 <div class="x_panel">
    <div class="table-responsive">
       <div class="x_title">
          <h4 class="margin_0">Live:</h4>
          <div class="clearfix"></div>
       </div>
       <table id="live_table" class="table table-bordered">
          <thead>
             <tr>
                <th width="5%">Order</th>
                <th>Date</th>
                <th>Start Time</th>
                <th>Series Name</th>
                <th>Match</th>
                <th>Match Type</th>
                <th>Status</th>
                <th>Add Match Card</th>
             </tr>
          </thead>
          <tbody>
          	<?php foreach ($this->object['data'] as $key => $matchDetails) { 
          		$date = date("d M", $matchDetails['start_date']['timestamp']); 
          		$time = date("H:i A", $matchDetails['start_date']['timestamp']); 

          		$series = $matchDetails['season']['name'];
          		$format = $matchDetails['format'];
          		$match  = $matchDetails['name'];
          	?>
             <tr>
                <td class="text-center">
                	<h3><a class="expandMatch expandQuiz accordionIcon" href="#" data-matchkey="<?php echo $matchDetails['key']; ?>" data-team-a="<?php echo $matchDetails['teams']['a']['name']; ?>" data-team-b="<?php echo $matchDetails['teams']['b']['name']; ?>" >
                    <i class="indicator rotate fa fa-plus-circle" data-toggle="collapse" data-target="#hide_<?php echo $matchDetails['key']; ?>" role="row"></i>
                    </a></h3>
                </td>
                <td><?php echo $date; ?></td>
                <td><?php echo $time; ?></td>
                <td><?php echo $series; ?></td>
                <td><?php echo $match; ?></td>
                <td><?php echo $format; ?></td>
                <td><?php echo $matchDetails['status']; ?></td>
                <td>
				          <button name="add-match-card" class="btn btn-primary btn-xs addImage" style="width:200px;" data-key="<?php echo $matchDetails['key']; ?>" data-type="match" data-toggle="modal" data-target=".add_image" class="expandPlayers accordionIcon" data-match-key="{%=o.matchKey%}" data-team-key="team_a">Add Match Card</button>
                   <input type="hidden" name="match-key" value="<?php echo $matchDetails['key']; ?>"/>
                </td>
             </tr>
             <tr id="hide_<?php echo $matchDetails['key']; ?>" class="accordion-body collapse"></tr>                     
  			<?php } ?>
          </tbody>
       </table>
    </div>
 </div>
</div>
</div>
<div class="modal fade add_image" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-lg">
<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
    </div>
    <div class="modal-body">
    <form type="POST" id="imageUpload">
      <table  class="table add_image_container table-bordered">
      <tbody>
          <tr>
              <td class="text-center">
                <div class="form-inline">
                    <label>Type:</label>
                    <select class="select2_single form-control" tabindex="-1" name="type">
                    <option value="match">Match</option>
                    <option value="team">Team</option>
                    <option value="player">Player</option>
                  </select>
                </div>          
              </td>
              <td class="text-center">
                <div class="form-inline">
                    <label>Key:</label>
                    <input class="form-control" name="key" placeholder="" value="">
                </div>
              </td>
              <td class="text-center">
                <div class="form-inline">
                    <label>File:</label>
                    <input class="form-control" type="file" name="imageToUpload" placeholder="" value="">
                </div>
              </td>
              <td><button type="submit" class="btn btn-primary btn-xs">Add Image</button></td>
          </tr>
      </tbody>
  </table>
  </form>
    </div>
</div>
</div>

</div>
<?php } ?>
<!-- List Match List End -->

<script type="text/x-tmpl" id="option_box">
<td colspan="9" class="no-padding bg_white_smoke">
    <div class="product_{%=o.matchKey%}" id="accordion1" data-match-key="{%=o.matchKey%}">
        <div >
            <table  class="table add_image_container">
                <tbody>
                    <tr>
                        <td width="34%" class="text-right"><a href="javascript:void(0);" data-toggle="modal" data-target=".add_image" class="expandPlayers accordionIcon" data-match-key="{%=o.matchKey%}" data-team-key="team_a">
                            <i class="fa fa-plus-circle"></i>
                            </a>
                        </td>
                        <td width="6%" >{%=o.team_a%}</td>
                        <td><button type="submit" class="btn btn-primary btn-xs" data-toggle="modal" data-target=".add_image" class="expandPlayers accordionIcon" data-match-key="{%=o.matchKey%}" data-team-key="team_a">Add Image</button></td>
                    </tr>
                    <tr>
                        <td width="34%" class="text-right"><a href="javascript:void(0);" data-toggle="modal" data-target=".add_image" class="expandPlayers accordionIcon" data-match-key="{%=o.matchKey%}" data-team-key="team_b">
                            <i class="fa fa-plus-circle"></i>
                            </a>
                        </td>
                        <td width="6%" >{%=o.team_b%}</td>
                        <td><button type="submit" class="btn btn-primary btn-xs" data-toggle="modal" data-target=".add_image" class="expandPlayers accordionIcon" data-match-key="{%=o.matchKey%}" data-team-key="team_a">Add Image</button></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</td>
</script>
<script type="text/x-tmpl" id="player_box">
	<!-- modal -->
        
            

        <!-- /modals -->
</script>
