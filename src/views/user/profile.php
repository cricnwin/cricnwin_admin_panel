<style type="text/css">
	.tile_count .tile_stats_count:before {
    margin-top: 0 !important;
    min-height: 150px;
}
</style><div class="row top_tiles" style="margin: 10px 0;">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 tile">
     <div class="left_col scroll-view">
		<div class="profile clearfix">
			<div class="profile_pic">
				<img src="public/img/img.jpg" alt="..." class="img-circle profile_img">
			</div>
			<div class="profile_info">
				<span>Welcome,</span>
				<h2>Venus Dharia (male)</h2>
			</div>
		</div>
	</div>
  </div>
</div>
<div class="row tile_count">
	<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 tile_stats_count ">
	    <label class="count_bottom text-center">ID:&nbsp;&nbsp;<i class="green"></i></label>
	    <div class="clearfix"></div>
	    <label class="count_bottom text-center">MAIL:&nbsp;&nbsp;<i class="green"></i></label>
	    <div class="clearfix"></div>
	    <label class="count_bottom text-center">PHONE:&nbsp;&nbsp;<i class="green"></i></label>
	    <div class="clearfix"></div>
	    <label class="count_bottom text-center">USERNAME:&nbsp;&nbsp;<i class="green"></i></label>
	</div>
	<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 tile_stats_count">
	    <label class="count_bottom text-center">SIGNUP-GMAIL:&nbsp;&nbsp;<i class="green"></i></label>
	    <div class="clearfix"></div><br/>
	    <label class="count_bottom text-center">LAST LOGIN-FB:&nbsp;&nbsp;<i class="green"></i></label>
	    <div class="clearfix"></div><br/>
	    <label class="count_bottom text-center">LOCATION:&nbsp;&nbsp;<i class="green"></i></label>
	    <div class="clearfix"></div><br/>
	</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 tile_stats_count">
		    <div class="input-group">
			  <span class="input-group-addon" style="display:table-cell;">COINS:&nbsp;</span>
			  <input type="text" class="form-control" aria-describedby="basic-addon1">
			</div>
		    <div class="input-group">
			  <span class="input-group-addon" style="display:table-cell;">Runs:&nbsp;&nbsp;&nbsp;&nbsp;</span>
			  <input type="text" class="form-control" aria-describedby="basic-addon1">
			</div>
		    <div class="clearfix"></div><br/>
	    	<label class="count_bottom text-center">Pending Rewards:&nbsp;&nbsp;<i class="green">0</i></label>
	</div>
</div>

<div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="table-responsive">
                	<div class="row">
                		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                			<div class="list-group">
							  <span class="list-group-item active" style="background: #2A3F54;">
							    STATS
							  </span>
							  <span class="list-group-item">Level</span>
							  <span class="list-group-item">Accuracy</span>
							  <span class="list-group-item">Trophies</span>
							  <span class="list-group-item">Followers</span>
							  <span class="list-group-item">Follow</span>
							  <span class="list-group-item">Fan club</span>
							  <span class="list-group-item">Coins Wins</span>
							  <span class="list-group-item">News Read</span>
							  <span class="list-group-item">Trivia Played</span>
							  <span class="list-group-item">Question Played</span>
							  <span class="list-group-item">Unclaimed</span>
							</div>
                		</div>
                		<div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                			<table id="live_table" class="table table-bordered">
	                        <thead>
	                            <tr style="background: #2A3F54;color: #fff;">
	                                <th width="5%">#</th>
	                                <th>Match</th>
	                                <th>Date</th>
	                                <th>Ques played</th>
	                                <th>Coins Staked</th>
	                                <th>Coins Won</th>
	                                <th>Boost Used</th>
	                                <th>Rank</th>
	                                <th>Accuracy</th>
	                                <th>Runs</th>
	                            </tr>
	                        </thead>
                        	<tbody>
                        		<tr>
                        			<td>1</td>
                        			<td>Ind vs SL</td>
                        			<td>5 Sep 2018</td>
                        			<td>20</td>
                        			<td>80078</td>
                        			<td>8921</td>
                        			<td>3</td>
                        			<td>7</td>
                        			<td>80%</td>
                        			<td>248</td>
                        		</tr>
                        		<tr>
                        			<td>2</td>
                        			<td>Ind vs SL</td>
                        			<td>5 Sep 2018</td>
                        			<td>20</td>
                        			<td>80078</td>
                        			<td>8921</td>
                        			<td>3</td>
                        			<td>7</td>
                        			<td>80%</td>
                        			<td>248</td>
                        		</tr>
                        		<tr>
                        			<td>3</td>
                        			<td>Ind vs SL</td>
                        			<td>5 Sep 2018</td>
                        			<td>20</td>
                        			<td>80078</td>
                        			<td>8921</td>
                        			<td>3</td>
                        			<td>7</td>
                        			<td>80%</td>
                        			<td>248</td>
                        		</tr>
                        		<tr>
                        			<td>4</td>
                        			<td>Ind vs SL</td>
                        			<td>5 Sep 2018</td>
                        			<td>20</td>
                        			<td>80078</td>
                        			<td>8921</td>
                        			<td>3</td>
                        			<td>7</td>
                        			<td>80%</td>
                        			<td>248</td>
                        		</tr>
                        		<tr>
                        			<td>5</td>
                        			<td>Ind vs SL</td>
                        			<td>5 Sep 2018</td>
                        			<td>20</td>
                        			<td>80078</td>
                        			<td>8921</td>
                        			<td>3</td>
                        			<td>7</td>
                        			<td>80%</td>
                        			<td>248</td>
                        		</tr>
                        		<tr>
                        			<td>6</td>
                        			<td>Ind vs SL</td>
                        			<td>5 Sep 2018</td>
                        			<td>20</td>
                        			<td>80078</td>
                        			<td>8921</td>
                        			<td>3</td>
                        			<td>7</td>
                        			<td>80%</td>
                        			<td>248</td>
                        		</tr>
                        		<tr>
                        			<td>7</td>
                        			<td>Ind vs SL</td>
                        			<td>5 Sep 2018</td>
                        			<td>20</td>
                        			<td>80078</td>
                        			<td>8921</td>
                        			<td>3</td>
                        			<td>7</td>
                        			<td>80%</td>
                        			<td>248</td>
                        		</tr>
                        		<tr>
                        			<td>8</td>
                        			<td>Ind vs SL</td>
                        			<td>5 Sep 2018</td>
                        			<td>20</td>
                        			<td>80078</td>
                        			<td>8921</td>
                        			<td>3</td>
                        			<td>7</td>
                        			<td>80%</td>
                        			<td>248</td>
                        		</tr>
                        		<tr>
                        			<td>9</td>
                        			<td>Ind vs SL</td>
                        			<td>5 Sep 2018</td>
                        			<td>20</td>
                        			<td>80078</td>
                        			<td>8921</td>
                        			<td>3</td>
                        			<td>7</td>
                        			<td>80%</td>
                        			<td>248</td>
                        		</tr>
                        		<tr>
                        			<td>10</td>
                        			<td>Ind vs SL</td>
                        			<td>5 Sep 2018</td>
                        			<td>20</td>
                        			<td>80078</td>
                        			<td>8921</td>
                        			<td>3</td>
                        			<td>7</td>
                        			<td>80%</td>
                        			<td>248</td>
                        		</tr>
                        		<tr>
                        			<td>11</td>
                        			<td>Ind vs SL</td>
                        			<td>5 Sep 2018</td>
                        			<td>20</td>
                        			<td>80078</td>
                        			<td>8921</td>
                        			<td>3</td>
                        			<td>7</td>
                        			<td>80%</td>
                        			<td>248</td>
                        		</tr>
                        		<tr>
                        			<td>12</td>
                        			<td>Ind vs SL</td>
                        			<td>5 Sep 2018</td>
                        			<td>20</td>
                        			<td>80078</td>
                        			<td>8921</td>
                        			<td>3</td>
                        			<td>7</td>
                        			<td>80%</td>
                        			<td>248</td>
                        		</tr>
                        		<tr>
                        			<td>13</td>
                        			<td>Ind vs SL</td>
                        			<td>5 Sep 2018</td>
                        			<td>20</td>
                        			<td>80078</td>
                        			<td>8921</td>
                        			<td>3</td>
                        			<td>7</td>
                        			<td>80%</td>
                        			<td>248</td>
                        		</tr>
                        	</tbody>
                        	</table>
                		</div>
                	</div>
                </div>
            </div>
        </div>
    </div>
</div>
