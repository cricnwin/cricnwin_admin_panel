<!-- page content -->

           <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                  <div class="table-responsive">
                        <table class="table table-striped jambo_table">
                          <thead>
                            <tr class="headings">
                              <th class="column-title" width="10%"> </th>
                              <th class="column-title" width="10%">A </th>
                              <th class="column-title" width="10%">B</th>
                              <th class="column-title" width="10%">C </th>
                              <th class="column-title" width="10%">D </th>
                              <th class="column-title" width="10%">E </th>
                              <th class="column-title" width="10%">F</th>
                              <th class="column-title" width="10%">G</th>
                              <th class="column-title" width="10%">H</th>
                            </tr>
                          </thead>

                          <tbody>
                            <tr class="even pointer">
                              <td class=""><b>Total</b></td>
                              <td class="">a</td>
                              <td class=" ">b</td>
                              <td class=" ">c </td>
                              <td class=" ">d</td>
                              <td class=" ">e</td>
                              <td class=" ">f</td>
                              <td class="">g</td>
                              <td class="">h</td>
                              
                            </tr>
                             <tr class="odd pointer">
                              <td class=""><b>Total</b></td>
                              <td class="">a</td>
                              <td class=" ">b</td>
                              <td class=" ">c </td>
                              <td class=" ">d</td>
                              <td class=" ">e</td>
                              <td class=" ">f</td>
                              <td class="">g</td>
                              <td class="">h</td>                            
                            </tr>

                          </tbody>
                        </table>
                   </div>
            </div>

             

            <div class="clearfix"></div>
            
             <div class="row">
                <div class="col-md-7 col-sm-7 col-xs-12">
                  <div class="x_panel">
                  <div class="table-responsive table_without_border">  
                     <table class="table">
                         
                          <tbody>
                            <tr>
                              <td class="text-center vertical_middle">Coins in System Currently</td>
                              <td width="30%"><input type="text" class="form-control" disabled="disabled" placeholder="Disabled Input"></td>
                            </tr>
                                                        
                            <tr>
                              <td class="text-center vertical_middle">Coins in System Currently</td>
                              <td width="30%"><input type="text" class="form-control" disabled="disabled" placeholder="Disabled Input"></td> 
                            </tr>
                           
                            
                          </tbody>
                        </table>
                  </div>
                   </div>   
                </div>

                 <div class="col-md-5 col-sm-5 col-xs-12">
                    <div class="x_panel">
                      <div class="x_title">
                        <h4 class="margin_0">System's Accuracy</h4>                        
                        <div class="clearfix"></div>
                      </div>
                      <div class="table-responsive table_without_border">
                       <table class="table">
                            
                            <tbody>
                              <tr>
                                <td><b>Total</b></td>
                                <td>Mark</td>
                                                        
                              </tr>
                              <tr>
                                <td><b>Today</b></td>
                                <td>Mark</td>
                                                        
                              </tr>
                             
                             
                              
                            </tbody>
                          </table>
                       </div>   
                     </div>   
                </div>
              </div>

              <div class="clearfix"></div>
               <div class="row">
                      <div class="col-md-4 col-sm-4 col-xs-12">
                          <div class="x_panel">
                              <div class="table-responsive">
                                  <div class="x_title">
                                      <h4 class="margin_0">Rewards Claimed</h4>
                                      <div class="clearfix"></div>
                                  </div>

                                  <table class="table table-bordered">

                                      <thead>
                                          <tr>
                                              <th></th>
                                              <th>First Name</th>
                                              <th>Last Name</th>

                                          </tr>
                                      </thead>
                                      <tbody>
                                          <tr>
                                              <td><b>Total</b></td>
                                              <td>Mark</td>
                                              <td>Otto</td>

                                          </tr>
                                          <tr>
                                              <td><b>Today</b></td>

                                              <td>Jacob</td>
                                              <td>Jacob</td>

                                          </tr>

                                      </tbody>
                                  </table>
                              </div>
                          </div>
                      </div>
                      <div class="col-md-4 col-sm-4 col-xs-12">
                          <div class="x_panel">
                              <div class="table-responsive">
                                  <div class="x_title">
                                      <h4 class="margin_0">Rewards Unclaimed</h4>
                                      <div class="clearfix"></div>
                                  </div>

                                  <table class="table table-bordered">

                                      <thead>
                                          <tr>

                                              <th>First Name</th>
                                              <th>Last Name</th>

                                          </tr>
                                      </thead>
                                      <tbody>
                                          <tr>

                                              <td>Mark</td>
                                              <td>Otto</td>

                                          </tr>
                                          <tr>

                                              <td>Jacob</td>
                                              <td>Jacob</td>

                                          </tr>

                                      </tbody>
                                  </table>
                              </div>
                          </div>
                      </div>

                      <div class="col-md-4 col-sm-4 col-xs-12">
                          <div class="x_panel">
                              <div class="x_title">
                                  <h4 class="margin_0">Users who won reward today</h4>
                                  <div class="clearfix"></div>
                              </div>
                              <div class="table-responsive">

                                  <table class="table table-bordered">

                                      <tbody>

                                          <tr>
                                              <td>#1</td>
                                              <td><span class="label label-default">Sanjay</span></td>
                                              <td>Claimed</td>
                                              <td class="text-center">
                                                  <button type="button" class="btn btn-success btn-xs">Pay</button>
                                              </td>
                                          </tr>
                                          <tr>
                                              <td>Top 1</td>
                                              <td><span class="label label-default">Sanjay</span></td>
                                              <td>Unclaimed</td>
                                              <td class="text-center">
                                                  <button type="button" class="btn btn-primary btn-xs">Push</button>
                                              </td>
                                          </tr>
                                          <tr>
                                              <td>#2</td>
                                              <td><span class="label label-default">Sanjay</span></td>
                                              <td>Claimed</td>
                                              <td class="text-center">
                                                  <button type="button" class="btn btn-success btn-xs">Pay</button>
                                              </td>
                                          </tr>
                                          <tr>
                                              <td>Top 2</td>
                                              <td><span class="label label-default">Sanjay</span></td>
                                              <td>Unclaimed</td>
                                              <td class="text-center">
                                                  <button type="button" class="btn btn-primary btn-xs">Push</button>
                                              </td>
                                          </tr>

                                      </tbody>
                                  </table>

                              </div>
                              <div class="ln_solid"></div>
                              <div class="clearfix">
                                  <a href="#" class="pull-right underline">Expand All</a>
                              </div>
                          </div>
                      </div>

              </div>

              <div class="clearfix"></div>
              <div class="row">
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h4 class="margin_0">User's at level</h4>
                                <div class="clearfix"></div>
                            </div>
                            <div class="table-responsive table_without_border">
                                <table class="table">

                                    <tbody>
                                        <tr>

                                            <td>Mark</td>
                                            <td>Otto</td>

                                        </tr>
                                        <tr>

                                            <td>Thornton</td>
                                            <td>@fat</td>
                                        </tr>
                                        <tr>

                                            <td>Thornton</td>
                                            <td>@fat</td>
                                        </tr>
                                        <tr>

                                            <td>Thornton</td>
                                            <td>@fat</td>
                                        </tr>

                                    </tbody>
                                </table>

                            </div>
                            <div class="ln_solid"></div>
                            <div class="clearfix">
                                <a href="#" class="pull-right underline">Expand All</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="x_panel">
                            <div class="table-responsive">
                                <table class="table table-bordered">

                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>First Name</th>
                                            <th>Last Name</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><b>Total</b></td>
                                            <td>Mark</td>
                                            <td>Otto</td>

                                        </tr>
                                        <tr>
                                            <td><b>Today</b></td>

                                            <td>Jacob</td>
                                            <td>Jacob</td>

                                        </tr>

                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>

                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="x_panel">
                            <div class="table-responsive">
                                <div class="x_title">
                                    <h4 class="margin_0">Coins Purchased</h4>
                                    <div class="clearfix"></div>
                                </div>

                                <table class="table table-bordered">

                                    <thead>
                                        <tr>

                                            <th>First Name</th>
                                            <th>Last Name</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>

                                            <td>Mark</td>
                                            <td>Otto</td>

                                        </tr>
                                        <tr>

                                            <td>Jacob</td>
                                            <td>Jacob</td>

                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

            </div>

              </div>

              <div class="clearfix"></div>
