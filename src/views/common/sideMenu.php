<?php 

$version = isset($_REQUEST['ver']) ? $_REQUEST['ver']: Config::$cssVersion;
$js_css_path = Config::$jsCssFolderPath;

?>
<div class="col-md-3 left_col">
  <div class="left_col scroll-view">
    <div class="navbar nav_title" style="border: 0;">
      <a href="dashboard" class="site_title text-center"><span>Cricnwin</span></a>
    </div>

    <div class="clearfix"></div>

    <br />

    <!-- sidebar menu -->
    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
      <div class="menu_section">
        <h3>General</h3>
        <ul class="nav side-menu">                                  
          <li><a href="dashboard"><i class="fa fa-home"></i>Dashboard</a></li>
          <li><a href="match-list"><i class="fa fa-puzzle-piece"></i>Quiz</a></li>
            <li><a href="match-schedule"><i class="fa fa-puzzle-piece"></i>Schedule</a></li>
            <li><a href="stories"><i class="fa fa-book"></i>Stories</a></li>
          <li><a href="add-image"><i class="fa fa-upload"></i>Upload Image</a></li>
          <li><a href="carousel-image"><i class="fa fa-upload"></i>Carousel Upload</a></li>
        </ul>
      </div>
    </div>
    <!-- /sidebar menu -->           
  </div>
</div>
