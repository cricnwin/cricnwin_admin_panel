<?php 

$version = isset($_REQUEST['ver']) ? $_REQUEST['ver']: time();
$js_css_path = Config::$jsCssFolderPath;

?>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Company Name </title>
<!-- Bootstrap -->
<link href="<?php echo $js_css_path; ?>assets/bootstrap/dist/css/bootstrap.min.css?ver=<?php echo $version; ?>" rel="stylesheet">
<!-- Custom Theme Style -->
<link href="<?php echo $js_css_path; ?>css/pnotify.custom.min.css?ver=<?php echo $version; ?>" rel="stylesheet">
<!-- Custom Theme Style -->
<link href="<?php echo $js_css_path; ?>css/jquery.loading.css?ver=<?php echo $version; ?>" rel="stylesheet">

<!-- Font Awesome -->
<link href="<?php echo $js_css_path; ?>assets/font-awesome/css/font-awesome.min.css?ver=<?php echo $version; ?>" rel="stylesheet">
<!-- NProgress -->
<link href="<?php echo $js_css_path; ?>assets/nprogress/nprogress.css?ver=<?php echo $version; ?>" rel="stylesheet">
<!-- iCheck -->
<link href="<?php echo $js_css_path; ?>assets/iCheck/skins/flat/green.css?ver=<?php echo $version; ?>" rel="stylesheet">
<!-- Datatables -->
<link href="<?php echo $js_css_path; ?>assets/datatables.net-bs/css/dataTables.bootstrap.min.css?ver=<?php echo $version; ?>" rel="stylesheet">
<link href="<?php echo $js_css_path; ?>assets/datatables.net-buttons-bs/css/buttons.bootstrap.min.css?ver=<?php echo $version; ?>" rel="stylesheet">
<link href="<?php echo $js_css_path; ?>assets/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css?ver=<?php echo $version; ?>" rel="stylesheet">
<link href="<?php echo $js_css_path; ?>assets/datatables.net-responsive-bs/css/responsive.bootstrap.min.css?ver=<?php echo $version; ?>" rel="stylesheet">
<link href="<?php echo $js_css_path; ?>assets/datatables.net-scroller-bs/css/scroller.bootstrap.min.css?ver=<?php echo $version; ?>" rel="stylesheet">
<!-- Select2 -->
<link href="<?php echo $js_css_path; ?>assets/select2/dist/css/select2.min.css?ver=<?php echo $version; ?>" rel="stylesheet">
<!-- bootstrap-daterangepicker -->
<link href="<?php echo $js_css_path; ?>assets/bootstrap-daterangepicker/daterangepicker.css?ver=<?php echo $version; ?>" rel="stylesheet">
<!-- bootstrap-datetimepicker -->
<link href="<?php echo $js_css_path; ?>assets/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css?ver=<?php echo $version; ?>" rel="stylesheet">
<link href="<?php echo $js_css_path; ?>css/bootstrap-multiselect.css?ver=<?php echo $version; ?>" rel="stylesheet">
<!-- Pnotify Theme Style -->
<link href="<?php echo $js_css_path; ?>css/custom.css?ver=<?php echo $version; ?>" rel="stylesheet">
