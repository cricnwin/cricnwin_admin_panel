<?php 

$version = isset($_REQUEST['ver']) ? $_REQUEST['ver']: time();
$action = empty($_REQUEST['action']) == false ? $_REQUEST['action'] : "dashboard";

$js_css_path = Config::$jsCssFolderPath;
$apiPath = Config::$apiEndPoint;

?>
<!-- jQuery -->
<script src="<?php echo $js_css_path; ?>assets/jquery/dist/jquery.min.js?v=<?php echo $version; ?>"></script>
<!-- Bootstrap -->
<script src="http://code.jquery.com/ui/1.10.0/jquery-ui.js"></script>
<script src="<?php echo $js_css_path; ?>js/jquery.validate.js"></script>
<script src="<?php echo $js_css_path; ?>assets/bootstrap/dist/js/bootstrap.min.js?v=<?php echo $version; ?>"></script>
<!-- NProgress -->
<script src="<?php echo $js_css_path; ?>assets/nprogress/nprogress.js?v=<?php echo $version; ?>"></script>
 <!-- Datatables -->
<script src="<?php echo $js_css_path; ?>js/datatables.min.js?v=<?php echo $version; ?>"></script>
<!-- Custom Theme Scripts -->
<script src="<?php echo $js_css_path; ?>js/custom.js?v=<?php echo $version; ?>"></script>
<script src="<?php echo $js_css_path; ?>js/moment.min.js?v=<?php echo $version; ?>"></script>
<script src="<?php echo $js_css_path; ?>js/bootstrap-datetimepicker.min.js?v=<?php echo $version; ?>"></script>
<script type="text/javascript" src="<?php echo $js_css_path; ?>js/tmpl.min.js?v=<?php echo $version; ?>"></script>
<script src="<?php echo $js_css_path; ?>js/bootstrap-imageupload.min.js?v=<?php echo $version; ?>"></script>
<script src="<?php echo $js_css_path; ?>js/pnotify.custom.min.js?v=<?php echo $version; ?>"></script>
<script src="<?php echo $js_css_path; ?>js/jquery.loading.js?v=<?php echo $version; ?>"></script>
<script src="<?php echo $js_css_path; ?>js/bootstrap-multiselect.js?v=<?php echo $version; ?>"></script>
<script src="<?php echo $js_css_path; ?>js/<?php echo $action; ?>_script.js?v=<?php echo $version; ?>" type="text/javascript" ></script>
<script type="text/javascript">
	apiEndPoint = "<?php echo $apiPath; ?>";
</script>