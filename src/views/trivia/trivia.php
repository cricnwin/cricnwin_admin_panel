                <!-- Match List Starts -->
<div class="row top_tiles" style="margin: 10px 0;">
  <div class="col-md-6 col-sm-6 col-xs-12 tile">
     <h2>Trivia</h2>
  </div>
</div>
<div class="row" style="margin: 10px 0;">
  <div class="col-md-4 col-sm-4 col-xs-8 tile pull-left">
    <span class="count text-center">Coins on Won: &nbsp;</span>
  </div>
</div>
<div class="row" style="margin: 10px 0;">
  <div class="col-md-4 col-sm-4 col-xs-8 tile pull-left">
    <span class="count text-center">Coins on Loss: &nbsp;</span>
  </div>
</div>
<!-- Match List End -->

<div class="row">
<div class="col-md-12 col-sm-12 col-xs-12">
 <div class="x_panel">
       <table id="live_table" class="table table-bordered dataTable no-footer" role="grid" aria-describedby="live_table_info">
          <thead>
             <th>Order</th>
             <th>S. No.</th>
             <th>Name</th>
             <th>Tags Match</th>
             <th>Graphics</th>
             <th>Status</th>
             <th>Questions</th>
             <th>Export/Import</th>
             <th>Action</th>
          </thead>
          <tbody>  
             <tr role="row" class="odd">
                <td class="text-center sorting_1"><img class="drag-icon" src="public/img/drag-small.png"></td>
                <td>1</td>
                <td>New Zealand's tour of India- Details!</td>
                <td></td>
                <td><span class="glyphicon glyphicon-picture" aria-hidden="true"></span></td>
                <td>
                    <span class="glyphicon glyphicon-ok" aria-hidden="true"></span> 
                </td>
                <td>20</td>
                <td>
                    <span class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span>
                    <span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span>
                </td>
                <td>
                  <form action="edit-story" method="POST">
                    <button type="submit" name="edit-story" class="btn btn-primary btn-xs">Edit</button>
                    <input type="hidden" name="story-key" value="308">
                  </form>   
                  <button type="button" class="btn btn-primary btn-xs">Remove</button>
                </td>
             </tr>
             <tr role="row" class="even">
                <td class="text-center sorting_1"><img class="drag-icon" src="public/img/drag-small.png"></td>
                <td>2</td>
                <td>How much does Cricket pay?</td>
                <td></td>
                <td><span class="glyphicon glyphicon-picture" aria-hidden="true"></span></td>
                <td>
                    <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>   
                </td>
                <td>20</td>
                <td><span class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span>
                    <span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span></td>
                <td>
                  <form action="edit-story" method="POST">
                   <button type="submit" name="edit-story" class="btn btn-primary btn-xs">Edit</button>
                    <input type="hidden" name="story-key" value="309">
                  </form>   
                  <button type="button" class="btn btn-primary btn-xs">Remove</button>
                </td>
             </tr><tr role="row" class="odd">
                <td class="text-center sorting_1"><img class="drag-icon" src="public/img/drag-small.png"></td>
                <td>3</td>
                <td>Indian women cricketers in Big Bash League</td>
                <td></td>
                <td><span class="glyphicon glyphicon-picture" aria-hidden="true"></span></td>
                <td>
                    <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>  
                </td>
                <td>20</td>
                <td><span class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span>
                    <span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span></td>
                <td>
                  <form action="edit-story" method="POST">
                   <button type="submit" name="edit-story" class="btn btn-primary btn-xs">Edit</button>
                    <input type="hidden" name="story-key" value="310">
                  </form>   
                  <button type="button" class="btn btn-primary btn-xs">Remove</button>
                </td>
             </tr><tr role="row" class="even">
                <td class="text-center sorting_1"><img class="drag-icon" src="public/img/drag-small.png"></td>
                <td>4</td>
                <td>Domestic violence case against Yuvraj Singh</td>
                <td></td>
                <td><span class="glyphicon glyphicon-picture" aria-hidden="true"></span></td>
                <td>
                    <span class="glyphicon glyphicon-ok" aria-hidden="true"></span> 
                </td>
                <td>20</td>
                <td><span class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span>
                    <span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span></td>
                <td>
                  <form action="edit-story" method="POST">
                   <button type="submit" name="edit-story" class="btn btn-primary btn-xs">Edit</button>
                    <input type="hidden" name="story-key" value="311">
                  </form>   
                  <button type="button" class="btn btn-primary btn-xs">Remove</button>
                </td>
             </tr><tr role="row" class="odd">
                <td class="text-center sorting_1"><img class="drag-icon" src="public/img/drag-small.png"></td>
                <td>5</td>
                <td>Four-day Tests, good or bad idea?</td>
                <td></td>
                <td><span class="glyphicon glyphicon-picture" aria-hidden="true"></span></td>
                <td>
                    <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>   
                </td>
                <td>20</td>
                <td><span class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span>
                    <span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span></td>
                <td>
                  <form action="edit-story" method="POST">
                   <button type="submit" name="edit-story" class="btn btn-primary btn-xs">Edit</button>
                    <input type="hidden" name="story-key" value="312">
                  </form>   
                  <button type="button" class="btn btn-primary btn-xs">Remove</button>
                </td>
             </tr><tr role="row" class="even">
                <td class="text-center sorting_1"><img class="drag-icon" src="public/img/drag-small.png"></td>
                <td>6</td>
                <td>Anil Kumble, captain, coach, and India's greatest match-winner</td>
                <td></td>
                <td><span class="glyphicon glyphicon-picture" aria-hidden="true"></span></td>
                <td>
                    <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>   
                </td>
                <td>20</td>
                <td><span class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span>
                    <span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span></td>
                <td>
                  <form action="edit-story" method="POST">
                   <button type="submit" name="edit-story" class="btn btn-primary btn-xs">Edit</button>
                    <input type="hidden" name="story-key" value="313">
                  </form>   
                  <button type="button" class="btn btn-primary btn-xs">Remove</button>
                </td>
             </tr><tr role="row" class="odd">
                <td class="text-center sorting_1"><img class="drag-icon" src="public/img/drag-small.png"></td>
                <td>7</td>
                <td>ICC T20 rankings: India eye top spot?</td>
                <td></td>
                <td><span class="glyphicon glyphicon-picture" aria-hidden="true"></span></td>
                <td>
                    <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>  
                </td>
                <td>20</td>
                <td><span class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span>
                    <span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span></td>
                <td>
                  <form action="edit-story" method="POST">
                   <button type="submit" name="edit-story" class="btn btn-primary btn-xs">Edit</button>
                    <input type="hidden" name="story-key" value="314">
                  </form>   
                  <button type="button" class="btn btn-primary btn-xs">Remove</button>
                </td>
             </tr><tr role="row" class="even">
                <td class="text-center sorting_1"><img class="drag-icon" src="public/img/drag-small.png"></td>
                <td>8</td>
                <td>Cricket's new Test Championship and ODI League</td>
                <td></td>
                <td><span class="glyphicon glyphicon-picture" aria-hidden="true"></span></td>
                <td>
                    <span class="glyphicon glyphicon-ok" aria-hidden="true"></span> 
                </td>
                <td>20</td>
                <td><span class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span>
                    <span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span></td>
                <td>
                  <form action="edit-story" method="POST">
                   <button type="submit" name="edit-story" class="btn btn-primary btn-xs">Edit</button>
                    <input type="hidden" name="story-key" value="315">
                  </form>   
                  <button type="button" class="btn btn-primary btn-xs">Remove</button>
                </td>
             </tr><tr role="row" class="odd">
                <td class="text-center sorting_1"><img class="drag-icon" src="public/img/drag-small.png"></td>
                <td>9</td>
                <td>Yuvraj's tussle with BCCI over money</td>
                <td></td>
                <td><span class="glyphicon glyphicon-picture" aria-hidden="true"></span></td>
                <td>
                    <span class="glyphicon glyphicon-ok" aria-hidden="true"></span> 
                </td>
                <td>20</td>
                <td><span class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span>
                    <span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span></td>
                <td>
                  <form action="edit-story" method="POST">
                   <button type="submit" name="edit-story" class="btn btn-primary btn-xs">Edit</button>
                    <input type="hidden" name="story-key" value="316">
                  </form>   
                  <button type="button" class="btn btn-primary btn-xs">Remove</button>
                </td>
             </tr><tr role="row" class="even">
                <td class="text-center sorting_1"><img class="drag-icon" src="public/img/drag-small.png"></td>
                <td>10</td>
                <td>Usman Khawaja's struggle with racism in Australia</td>
                <td></td>
                <td><span class="glyphicon glyphicon-picture" aria-hidden="true"></span></td>
                <td>
                    <span class="glyphicon glyphicon-ok" aria-hidden="true"></span> 
                </td>
                <td>20</td>
                <td><span class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span>
                    <span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span></td>
                <td>
                  <form action="edit-story" method="POST">
                   <button type="submit" name="edit-story" class="btn btn-primary btn-xs">Edit</button>
                    <input type="hidden" name="story-key" value="317">
                  </form>   
                  <button type="button" class="btn btn-primary btn-xs">Remove</button>
                </td>
             </tr></tbody>
       </table>
    </div>
 </div>
</div>
