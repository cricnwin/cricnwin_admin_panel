<?php  ?>
<!-- /top navigation -->
<!-- page content -->
<div class="row tile_count">
    <div class="col-md-2 col-sm-2 col-xs-12 tile_stats_count ">
        <div class="count text-left">View Claim List</div>
<!--        <span class="count_bottom text-center">Total Question:&nbsp;&nbsp;<i class="green">--><?php //echo $this->object['questionsTotal']; ?><!--</i></span>-->
<!--        <span class="count_bottom text-center">Live Question:&nbsp;&nbsp;<i class="green">--><?php //echo $this->object['questionsActive']; ?><!--</i></span>-->
    </div>
    <div class="col-md-2 col-sm-2 col-xs-12 tile pull-left">
<!--        <form action="add-quiz" method="POST">-->
<!--            <button type="submit" name="add-quiz" style="margin: 18% 0 0 20%;" class="btn btn-primary"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>&nbsp;Add Quiz</button>-->
<!--            <input type="hidden" name="match-key" value="--><?php //echo $_REQUEST['match-key']; ?><!--"/>-->
<!--        </form>-->
    </div>
    <div class="col-md-8 col-sm-8 col-xs-12 tile_stats_count">
        <div class="count">-- Match Name --</div>
        <div class="count">-- Match Status --</div>
    </div>
</div>
<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="table-responsive">
                <div class="x_title">
                    <h4 class="margin_0">Live:</h4>
                    <div class="clearfix"></div>
                </div>
                <table id="live_table" class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Rank</th>
                        <th>User Name</th>
                        <th>Email</th>
                        <th>Phone number</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach ($this->claimList as $key => $claimDetails) {
                        $rank = $claimDetails['rank'];
                        $username = $claimDetails['username'];
                        $email  = $claimDetails['email'];
                        $phoneNumber = $claimDetails['phoneNumber'];
                        ?>
                        <tr>
                            <td><?php echo $rank; ?></td>
                            <td><?php echo $username; ?></td>
                            <td><?php echo $email; ?></td>
                            <td><?php echo $phoneNumber; ?></td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
