<?php

$action = empty($_REQUEST['action']) == false ? $_REQUEST['action'] : "dashboard";
?>

<!-- Match List Starts -->
<div class="row top_tiles" style="margin: 10px 0;">
    <div class="col-md-6 col-sm-6 col-xs-6 tile">
        <h2>Schedule List</h2>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-6 tile">
        <span>Live Match:</span>
        <h2><?php echo count($this->liveMatchList); ?></h2>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-6 tile">
        <span>Upcoming Match:</span>
        <h2><?php echo count($this->upcomingMatchList); ?></h2>
    </div>
</div>

<div class="clearfix"></div>

<!-- Live Match List Starts -->
<?php if(count($this->liveMatchList) > 0) { ?>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="table-responsive">
                    <div class="x_title">
                        <h4 class="margin_0">Live:</h4>
                        <div class="clearfix"></div>
                    </div>
                    <table id="live_table" class="table table-bordered">
                        <thead>
                        <tr>
                            <th width="5%">Order</th>
                            <th>Date</th>
                            <th>Start Time</th>
                            <th>Series Name</th>
                            <th>Match</th>
                            <th>Match Key</th>
                            <th>Match Type</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($this->liveMatchList as $key => $matchDetails) {
                            $date = date("d M", strtotime($matchDetails['matchDateIst']));
                            $time = date("H:i A", strtotime($matchDetails['matchTimeIst']));

                            $series = $matchDetails['seriesName'];
                            $format = $matchDetails['type'];
                            $match  = $matchDetails['homeTeamFullName'] . " vs. " . $matchDetails['awayTeamFullName'] . ", " . $matchDetails['matchNumber'];
                            $matchKey = $matchDetails['id'];
                            ?>
                            <tr>
                                <td class="text-center"><img class="drag-icon" src="public/img/drag-small.png"/></td>
                                <td><?php echo $date; ?></td>
                                <td><?php echo $time; ?></td>
                                <td><?php echo $series; ?></td>
                                <td><?php echo $match; ?></td>
                                <td><?php echo $matchKey; ?></td>
                                <td><?php echo $format; ?></td>
                                <td>

                                    <form action="add-quiz" method="POST">
                                        <button type="submit" name="add-quiz" class="btn btn-primary btn-xs">Add Quiz</button>
                                        <input type="hidden" name="match-key" value="<?php echo $matchDetails['id']; ?>"/>
                                    </form>
                                    <form action="view-quiz" method="POST">
                                        <button type="submit" class="btn btn-primary btn-xs">View Quiz</button>
                                        <input type="hidden" name="match-key" value="<?php echo $matchDetails['id']; ?>"/>
                                    </form>
                                    <form action="" name="match-call-off" method="POST">
                                        <button type="submit" class="btn btn-primary btn-xs">Match Call-Off</button>
                                        <input type="hidden" name="match-key" value="<?php echo $matchDetails['id']; ?>"/>
                                        <input type="hidden" name="action" value="quiz"/>
                                        <input type="hidden" name="step" value="callOffMatch"/>
                                    </form>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
<!-- List Match List End -->

<div class="clearfix"></div>

<!-- Upcoming Match List Starts -->
<?php if(count($this->upcomingMatchList) > 0) { ?>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="table-responsive">
                    <div class="x_title">
                        <h4 class="margin_0">Upcoming:</h4>
                        <div class="clearfix"></div>
                    </div>
                    <table id="upcoming_table" class="table table-bordered">
                        <thead>
                        <tr>
                            <th width="5%">Order</th>
                            <th>Date</th>
                            <th>Start Time</th>
                            <th>Series Name</th>
                            <th>Match</th>
                            <th>Match Key</th>
                            <th>Match Type</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($this->upcomingMatchList as $key => $matchDetails) {
                            $date = date("d M", strtotime($matchDetails['matchDateIst']));
                            $time = date("H:i A", strtotime($matchDetails['matchTimeIst']));

                            $series = $matchDetails['seriesName'];
                            $format = $matchDetails['type'];
                            $match  = $matchDetails['homeTeamFullName'] . " vs. " . $matchDetails['awayTeamFullName'] . ", " . $matchDetails['matchNumber'];
                            $matchKey = $matchDetails['id'];

                            ?>
                            <tr>
                                <td class="text-center"><img class="drag-icon" src="public/img/drag-small.png"/></td>
                                <td><?php echo $date; ?></td>
                                <td><?php echo $time; ?></td>
                                <td><?php echo $series; ?></td>
                                <td><?php echo $match; ?></td>
                                <td><?php echo $matchKey; ?></td>
                                <td><?php echo $format; ?></td>
                                <td>
                                    <form action="add-quiz" method="POST">
                                        <button type="submit" name="add-quiz" class="btn btn-primary btn-xs">Add Quiz</button>
                                        <input type="hidden" name="match-key" value="<?php echo $matchDetails['id']; ?>"/>
                                    </form>
                                    <form action="view-quiz" method="POST">
                                        <button type="submit" class="btn btn-primary btn-xs">View Quiz</button>
                                        <input type="hidden" name="match-key" value="<?php echo $matchDetails['id']; ?>"/>
                                    </form>
                                    <form action="" name="match-call-off" method="POST">
                                        <button type="submit" class="btn btn-primary btn-xs">Match Call-Off</button>
                                        <input type="hidden" name="match-key" value="<?php echo $matchDetails['id']; ?>"/>
                                        <input type="hidden" name="action" value="quiz"/>
                                        <input type="hidden" name="step" value="disableMatch"/>
                                        <input type="hidden" name="status" value="CALLEDOFF"/>
                                    </form>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
<!-- Upcoming Match List Ends -->

<!-- Completed Match List Starts -->
<?php if(count($this->completedMatchList) > 0) { ?>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="table-responsive">
                    <div class="x_title">
                        <h4 class="margin_0">Past:</h4>
                        <div class="clearfix"></div>
                    </div>
                    <table id="upcoming_table" class="table table-bordered">
                        <thead>
                        <tr>
                            <th width="5%">Order</th>
                            <th>Date</th>
                            <th>Start Time </th>
                            <th>Series Name</th>
                            <th>Match</th>
                            <th>Match Key</th>
                            <th>Match Type</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($this->completedMatchList as $key => $matchDetails) {
                            $date = date("Y-m-d", strtotime($matchDetails['matchDateIst']));
                            $time = date("H:i A", strtotime($matchDetails['matchTimeIst']));

                            $series = $matchDetails['seriesName'];
                            $format = $matchDetails['type'];
                            $match  = $matchDetails['homeTeamFullName'] . " vs. " . $matchDetails['awayTeamFullName'] . ", " . $matchDetails['matchNumber'];
                            $matchKey = $matchDetails['id'];

                            ?>
                            <tr>
                                <td class="text-center"><img class="drag-icon" src="public/img/drag-small.png"/></td>
                                <td><?php echo $date; ?></td>
                                <td><?php echo $time; ?></td>
                                <td><?php echo $series; ?></td>
                                <td><?php echo $match; ?></td>
                                <td><?php echo $matchKey; ?></td>
                                <td><?php echo $format; ?></td>
                                <td>
                                    <form action="view-quiz" method="POST">
                                        <button type="submit" class="btn btn-primary btn-xs">View Quiz</button>
                                        <input type="hidden" name="match-key" value="<?php echo $matchDetails['id']; ?>"/>
                                    </form>
                                    <form action="view-claimlist" method="POST">
                                        <button type="submit" class="btn btn-primary btn-xs">Claim List</button>
                                        <input type="hidden" name="match-key" value="<?php echo $matchDetails['id']; ?>"/>
                                    </form>
                                    <button type="button" class="btn btn-primary btn-xs">Leaderboard</button>
                                    <button type="button" class="btn btn-primary btn-xs">Trophy</button>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
<!-- Completed Match List Ends -->

