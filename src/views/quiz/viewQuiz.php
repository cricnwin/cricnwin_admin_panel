<?php  ?>
<!-- /top navigation -->
<!-- page content -->
    <div class="row tile_count">
        <div class="col-md-2 col-sm-2 col-xs-12 tile_stats_count ">
            <div class="count text-left">View Quiz</div>
            <span class="count_bottom text-center">Total Question:&nbsp;&nbsp;<i class="green"><?php echo $this->object['questionsTotal']; ?></i></span>
            <span class="count_bottom text-center">Live Question:&nbsp;&nbsp;<i class="green"><?php echo $this->object['questionsActive']; ?></i></span>
        </div>
        <div class="col-md-2 col-sm-2 col-xs-12 tile pull-left">
            <form action="add-quiz" method="POST">
             <button type="submit" name="add-quiz" style="margin: 18% 0 0 20%;" class="btn btn-primary"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>&nbsp;Add Quiz</button>
             <input type="hidden" name="match-key" value="<?php echo $_REQUEST['match-key']; ?>"/>
            </form>
        </div>
        <div class="col-md-8 col-sm-8 col-xs-12 tile_stats_count">
            <div class="count">-- Match Name --</div>
            <div class="count">-- Match Status --</div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="table-responsive">
                    <table id="live_table" class="table table-bordered">
                        <thead>
                            <tr>
                                <th width="5%">Order</th>
                                <th>S.No.</th>
                                <th>Ques</th>
                                <th>Option A</th>
                                <th>Option B</th>
                                <th>Option C</th>
                                <th>Option D</th>
                                <th>Option E</th>
                                <th>Option F</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($this->object['quiz'] as $key => $quizDetails) { ?>
                        	
                            <tr>
                                <td class="text-center"><span class="glyphicon glyphicon-asterisk" aria-hidden="true"></span>
                                <input type="hidden" name="quizId" value="<?php echo $quizDetails['questionId']; ?>"/>
                                <input type="hidden" name="match-key" value="<?php echo $_REQUEST['match-key']; ?>"/>
                                </td>
                                <td><?php echo $key+1; ?></td>
                                <td><?php echo $quizDetails['questionText']; ?> <a class="expandQuiz accordionIcon" href="#" data-quizId="<?php echo $quizDetails['questionId']; ?>" data-matchkey="<?php echo $_REQUEST['match-key']; ?>" class="expandQuiz">
                                    <i class="accordion-toggle indicator rotate fa fa-plus-circle" data-toggle="collapse" data-parent="#collapse_table" data-target=".product_<?php echo $quizDetails['questionId']; ?>" role="row"></i>
                                    </a>
                                </td>
                                <?php for($i = 0; $i<6;$i++) { 
                                	$optionDetails = $quizDetails['options'][$i]; 
                                	if(empty($optionDetails) == false) { ?>
		                                <td>
		                                	<input type="hidden" name="optionId" value="<?php echo $optionDetails['optionId']; ?>"/>
		                                    <?php echo $optionDetails['optionText']; ?>
		                                    <hr class="custom_hr">
		                                    <span class="probability"><?php echo $optionDetails['optionProbability']; ?>
		                                    <i class="fa fa-pencil"></i>
		                                    </span>
		                                    <hr class="custom_hr">
		                                    <?php echo empty($optionDetails['totalCoinsBid']) ? 0 : $optionDetails['totalCoinsBid']; ?>  Coins
		                                    <hr class="custom_hr">
		                                    <div class="isAnswerLabel"><?php echo $optionDetails['isAnswer'] == 1 ? "selected" : "" ?></div>
                                            <div class="isAnswerCheckBox form-control" style="display: none;"><label><input type="checkbox" name="isAnswerCheck" value="select"/>&nbsp;select</label></div>
		                                </td>
		                            <?php } else {
		                            	echo "<td>-</td>";
		                            }
                                 } ?>
                                 <td>
                                 <span class="quizStatus"><?php echo $quizDetails['questionStatus']; ?></span>
                                 </td>
                                <td>
                                    <button type="submit" class="addAnswer btn btn-primary btn-xs">Add Answer</button>
                                    <button type="submit" class="disableQuiz btn btn-primary btn-xs"><?php if($quizDetails['questionStatus'] == "ACTIVE") { ?>Disable Quiz<?php } else { ?>Enable Quiz<?php } ?></button>
                                    <button type="submit" class="callOffQuiz btn btn-primary btn-xs">Call Off Question</button>
                                    <button type="submit" data-status="DELETED" class="disableQuiz btn btn-primary btn-xs">Delete Question</button>


                                </td>
                            </tr>
                            <tr id="hide_<?php echo $quizDetails['questionId']; ?>" class="hiddenRow"></tr>
                            <?php } ?>
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>

    <script type="text/x-tmpl" id="option_box">
            <td colspan="10" class="no-padding bg_white_smoke">
                <div class="accordion-body collapse product_{%=o.questionId%} accordionBody">
                    <div>
                        <table  class="table table-bordered">
                            <thead>
                                <tr>
                                	<th width="5%">S.No.</th>
                                    <th> Total</th>
                                    <th> Coins staked</th>
									<th> Runs collected</th>
									<th> Coins Earned</th>
									<th> Option selected</th>
									<th> rate</th>
									<th> Time</th>
									<th> B1</th>
									<th> B2</th>
									<th> Trade</th>
									<th> Ask a friend</th>
									<th> Hint</th>
                                </tr>
                            </thead>
                            <tbody>
                            {% for(var i=0; i<o.questionDetails.length; i++) { %}
                                <tr>
                                    <td class="text-center">{%=i%}</td>
                                    <td>User {%=i%}</td>
                                    <td>{%=o.questionDetails[i].bidValue%}</td>
                                    <td>{%=o.questionDetails[i].runsEarned%}</td>
                                    <td>{%=o.questionDetails[i].coinsEarned%}</td>
                                    <td>{%=o.questionDetails[i].optionId%}</td>
                                    <td>{%=o.questionDetails[i].optionValue%}</td>
                                    <td>-</td>
                                    <td>-</td>
                                    <td>-</td>
                                    <td>-</td>
                                    <td>-</td>
                                    <td>{%=o.questionDetails[i].runsEarned%}</td>
                                </tr>
                            {% } %}
                            </tbody>
                        </table>
                    </div>
                </div>
            </td>
    </script>
