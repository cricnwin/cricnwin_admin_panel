

<?php
 $matchDate = date("jS F Y", strtotime($this->object['startDate']));
?>
            <div class="row tile_count">
                <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                    <div class="count">Add Quiz</div>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                    <div class="count">Live Quiz:15</div>
                </div>
                <div class="col-md-6 col-sm-4 col-xs-12 tile_stats_count">
                    <div class="custom-text-wrap"><?php echo $this->object['name'].", ".$this->object['venue'].", ".$matchDate; ?></div>
                </div>
            </div>
            <form action="" method="POST" id="add_quiz">
            <div class="clearfix"></div>
            <div class="row">
                <div class="form-group">
                    <div data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
                        <label class="control-label col-md-1 col-sm-1 col-xs-2" for="first-name">Ques:
                        </label>
                        <div class="col-md-11 col-sm-10 col-xs-10  top_search">
                            <div class="input-group" style="width: 99%;">
                                <input type="text" name="formData[question][text]" class="form-control" placeholder="Type Quiz..." required>
                            </div>
                        </div>
                        
                        <input type="hidden" name="match-key" value="<?php echo $_REQUEST['match-key']; ?>"/>
                        <input type="hidden" name="formData[question][type]" value="play"/>
                        <input type="hidden" name="formData[question][status]" value="ACTIVE"/>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="row margin_bottom_20">
                            <div class="form-inline">
                                <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <label>Hint:</label>
                                    <select class="select2_single form-control" id="hintType" name="formData[question][hintType]">
                                        <option value="text">Text</option>
                                        <option value="image">Image</option>
                                    </select>
                                    <div id="hint_box" style="display:inline; width: 65%;">
                                        <textarea style="height: 34px;" name="formData[question][hint]"></textarea>
                                        <input class="form-control" style="display: none; width: 65%;" type="file" placeholder="Text/Image">
                                    </div>
                                </div>
                                <div class="form-group col-lg-2 col-md-2 col-sm-2 col-xs-5">
                                    <label>Runs:</label>
                                    <input class="form-control width_80" name="formData[question][runs]" placeholder="" value="0">
                                </div>
                                <div class="form-group col-lg-2 col-md-2 col-sm-2 col-xs-5">
                                    <label>Tag:</label>
                                    <select class="select2_single form-control"  tabindex="-1">
                                        <option>Select</option>
                                        <option>Bat</option>
                                        <option>Ball</option>
                                    </select>
                                </div>
                                <div class='form-group col-lg-4 col-md-4 col-sm-4 col-xs-7'>
                                    <label>Time:</label>
                                    <div class="form-group">
                                        <div class="input-group clockpicker" id="clockpicker">
                                            <input type="text" class="form-control">
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-time"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row" name="condition_container">
                            
                        </div>
                        <div class="clearfix"></div>
                        <div class="clearfix"></div>
                        <div class="row margin_bottom_20">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <a href="#" id="addbutton"> 
                                <span class="glyphicon glyphicon-plus margin_right_5" aria-hidden="true"></span>
                                <button type="button" class="btn btn-primary">Add Condition</button>
                                </a>   
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="row margin_bottom_30">
                            <div class="form-inline">
                                <div class="form-group col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                    <label class="padding_top_5 pull-left margin_right_5">No. of option:</label>
                                    <input type="number" min="0" max="6" id="nOfOption" class="form-control width_80 pull-left" placeholder="enter between 0 to 6" value="2">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="table-responsive">
                                    <table class="table" id="optionTable">
                                        <thead>
                                            <tr>
                                                <th>No.</th>
                                                <th width="40%">Option</th>
                                                <th>Probability</th>
                                                <th>Is Answer?</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                            <td><label class="padding_top_5 margin_left_5"><b>1</b></label></td>
                                            <td><input type="text" class="form-control" name="formData[options][0][text]" placeholder="Text/Value"></td>
                                            <td><input type="text" name="formData[options][0][probability]" class="form-control width_80 pull-left" placeholder="">
                                                <label class="padding_top_5 margin_left_5">%</label>
                                            </td>
                                            <td>
                                               <span class="input-group-addon">
                                                <input class="isAnswer"  type="checkbox" name="formData[options][0][isAnswer]" aria-label="Checkbox for following text input">
                                              </span></td>
                                        </tr>
                                        <tr>
                                            <td><label class="padding_top_5 margin_left_5"><b>2</b></label></td>
                                            <td><input type="text" class="form-control" name="formData[options][1][text]" placeholder="Text/Value"></td>
                                            <td><input type="text" name="formData[options][1][probability]" class="form-control width_80 pull-left" placeholder="">
                                                <label class="padding_top_5 margin_left_5">%</label>
                                            </td>
                                            <td>
                                               <span class="input-group-addon">
                                                <input class="isAnswer"  type="checkbox" name="formData[options][1][isAnswer]" aria-label="Checkbox for following text input">
                                              </span></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="ln_solid"></div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="row">
                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                <button type="button" class="btn btn-success sumbitForm" data-key="<?php echo $_REQUEST['match-key']; ?>" data-redirect="match" >Submit</button>
                            </div>
                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                <button type="button" name="add-quiz" class="btn btn-success sumbitForm" data-key="<?php echo $_REQUEST['match-key']; ?>" data-redirect="addAnother">Submit & Add Another</button>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            </form>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<script type="text/x-tmpl" id="player_box">
    <div class="x_title">
        <h4 class="margin_0">Condition <span name="cond-count">{% if(o.count) { print(o.count) } else { print(1) } %}</span>:</h4>
        <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>
    <div style="margin-left: 20px;" name="condition_box">
        <input type="hidden" class="count" value="{% if(o.count) { print(o.count) } else { print(1) } %}">
        <div class="row ">
            <div class="form-inline">
                <div class="form-group col-lg-1 col-md-1 col-sm-1 col-xs-3">
                    <select class="select2_single form-control team_select" tabindex="-1" name="team_select">
                        <option value="team">Teams</option>
                        <option value="player" selected>Players</option>
                    </select>
                </div>
                <div class="form-group col-lg-1 col-md-1 col-sm-1 col-xs-3">
                    <select class="select2_single form-control" tabindex="-1" name="formData[conditions][player][{%=o.count-1%}][team]">
                        <option value="<?php echo $this->object['teams']['a']['id']; ?>"><?php echo $this->object['teams']['a']['name']; ?></option>
                        <option value="<?php echo $this->object['teams']['b']['id']; ?>"><?php echo $this->object['teams']['b']['name']; ?></option>
                    </select>
                </div>
                <div class="form-group col-lg-1 col-md-1 col-sm-1 col-xs-3" >
                    <select class="select2_single form-control" tabindex="-1" name="formData[conditions][player][{%=o.count-1%}][innings]">
                        <option value="1">Innings 1</option>
                        <option value="2">Innings 2</option>
                    </select>
                </div>
                <div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-6">
                    <label>Player</label>
                    <select style="margin-left: 10px;" class="select2_single form-control select_factor" tabindex="-1" name="formData[conditions][player][{%=o.count-1%}][key]">
                       <?php foreach ($this->object['teams']['a']['players'] as $key => $value) { ?>
                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                        <?php } ?>
                        <?php foreach ($this->object['teams']['b']['players'] as $key => $value) { ?>
                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="form-group col-lg-2 col-md-2 col-sm-2 col-xs-6">
                    <label>Acting As:</label>
                    <select style="margin-left: 10px;" class="select2_single form-control select_factor" tabindex="-1" name="formData[conditions][player][{%=o.count-1%}][acting_as]">
                        <option value="batsman">Batsman</option>
                        <option value="bowler">Bowler</option>
                        <option value="fielding">Fielder</option>
                    </select>
                </div>
                <div class="form-group col-lg-2 col-md-2 col-sm-2 col-xs-6 factor_box">
                    <label>Factor</label>
                    <select style="margin-left: 10px;" class="select2_single form-control select_player_condition" name="formData[conditions][player][{%=o.count-1%}][factor]" style="display: inline;">
                    <?php foreach ($this->object['factors']['player']['batsmen'] as $key => $value) { ?>
                        <option value="_<?php echo $key; ?>_"><?php echo $value; ?></option>
                    <?php } ?>
                    </select>
                </div>
                <div class="form-group col-lg-2 col-md-2 col-sm-2 col-xs-6">
                    <select class="select2_single form-control condition_select_box" tabindex="-1" name="formData[conditions][player][{%=o.count-1%}][condition]">
                        <option>Condition</option>
                        <option value="><">Lies b/w</option>
                        <option value="<">Less Then</option>
                        <option value=">">More Then</option>
                    </select>
                    <a href="javascript:void(0);" class="removeCond"><i class="accordion-toggle indicator rotate fa fa-minus-circle" data-toggle="collapse" data-parent="#collapse_table" data-target=".product_<?php echo $quizDetails['questionId']; ?>" role="row" style="padding: 10px 10px 20px 0;"></i></a>
                </div>
            </div>
        </div>
        <div name="copy_container"></div>
    </div>
</script>
<script type="text/x-tmpl" id="team_box">
    <div class="x_title">
        <h4 class="margin_0">Condition <span name="cond-count">{% if(o.count) { print(o.count) } else { print(1) } %}</span>:</h4>
        <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>
    <div style="margin-left: 20px;" name="condition_box">
        <input type="hidden" class="count" value="{% if(o.count) { print(o.count) } else { print(1) } %}">
        <div class="row">
            <div class="form-inline">
                <div class="form-group col-lg-2 col-md-2 col-sm-2 col-xs-6">
                    <select class="select2_single form-control team_select" tabindex="-1" name="team_select">
                        <option value="team">Teams</option>
                        <option value="player">Players</option>
                    </select>
                </div>
                <div class="form-group col-lg-2 col-md-2 col-sm-2 col-xs-6">
                    <select class="select2_single form-control" tabindex="-1" name="formData[conditions][team][{%=o.count-1%}][team]">
                        <option value="<?php echo $this->object['teams']['a']['id']; ?>"><?php echo $this->object['teams']['a']['name']; ?></option>
                        <option value="<?php echo $this->object['teams']['b']['id']; ?>"><?php echo $this->object['teams']['b']['name']; ?></option>
                    </select>
                </div>
                <div class="form-group col-lg-2 col-md-2 col-sm-2 col-xs-6" >
                    <select class="select2_single form-control" tabindex="-1" name="formData[conditions][team][{%=o.count-1%}][innings]">
                        <option value="1">Innings 1</option>
                        <option value="2">Innings 2</option>
                    </select>
                </div>
                <div class="form-group col-lg-2 col-md-2 col-sm-2 col-xs-6" >
                    <select class="select2_single form-control" tabindex="-1" name="formData[conditions][team][{%=o.count-1%}][status]">
                        <option value="live">Ongoing</option>
                        <option value="past">Completed</option>
                    </select>
                </div>
                <div class="form-group col-lg-2 col-md-2 col-sm-2 col-xs-6">
                    <select class="select2_single form-control" tabindex="-1" name="formData[conditions][team][{%=o.count-1%}][factor]">
                        <option>Select Factor</option>
                        <?php foreach ($this->object['factors']['team'] as $key => $value) { ?>
                            <option value="_<?php echo $key; ?>_"><?php echo $value; ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="form-group col-lg-2 col-md-2 col-sm-2 col-xs-6">
                    <select class="select2_single form-control condition_select_box" tabindex="-1" name="formData[conditions][team][{%=o.count-1%}][condition]">
                        <option>Condition</option>
                        <option value="><">Lies b/w</option>
                        <option value="<">Less Then</option>
                        <option value=">">More Then</option>
                    </select>
                    <a href="javascript:void(0);" class="removeCond"><i class="accordion-toggle indicator rotate fa fa-minus-circle" data-toggle="collapse" data-parent="#collapse_table" data-target=".product_<?php echo $quizDetails['questionId']; ?>" role="row" style="padding: 10px 10px 20px 0;"></i></a>
                </div>
            </div>
        </div>
        <div name="copy_container"></div>
    </div>
</script>
<script type="text/x-tmpl" id="param_box">
    {% if(o.lies_btw) { %}
        <div class="row margin_bottom_20">
            <div class="form-inline">
                <div class="form-group col-lg-8 col-md-8 col-sm-8 col-xs-12">
                    <label class="padding_top_5 pull-left margin_right_5 padding_right_15">Lies between</label>
                    <input class="form-control width_80 pull-left" name="formData[conditions][{%=o.type%}][{%=o.count-1%}][minValue]" placeholder="Value">
                    <label class="margin_left_right_5 padding_top_5 pull-left padding_left_right_15">to</label>
                    <input class="form-control width_80 padding_top_5 pull-left" name="formData[conditions][{%=o.type%}][{%=o.count-1%}][maxValue]" placeholder="Value">
                </div>
            </div>
        </div>
    {% } else if(o.less_than) { %}
        <div class="row margin_bottom_20">
            <div class="form-inline">
                <div class="form-group col-lg-8 col-md-8 col-sm-8 col-xs-12">
                    <label class="padding_top_5 pull-left margin_right_5 padding_right_15">If less than</label>
                    <input class="form-control width_80 pull-left padding_right_15" name="formData[conditions][{%=o.type%}][{%=o.count-1%}][value]" placeholder="Value"/>
                </div>
            </div>
        </div>
    {% } else { %}
        <div class="row margin_bottom_20">
            <div class="form-inline">
                <div class="form-group col-lg-8 col-md-8 col-sm-8 col-xs-12">
                    <label class="padding_top_5 pull-left margin_right_5 padding_right_15">If more than</label>
                    <input class="form-control width_80 pull-left padding_right_15" name="formData[conditions][{%=o.type%}][{%=o.count-1%}][value]" placeholder="Value"/>
                </div>
            </div>
        </div>
    {% } %}
</script>
<script type="text/x-tmpl" id="option_box">
    <tr>
        <td><label class="padding_top_5 margin_left_5"><b>{%=o.count+1%}</b></label></td>
        <td><input type="text" class="form-control" name="formData[options][{%=o.count%}][text]" placeholder="Text/Value"></td>
        <td><input type="text" name="formData[options][{%=o.count%}][probability]" class="form-control width_80 pull-left" placeholder="">
            <label class="padding_top_5 margin_left_5">%</label>
        </td>
        <td>
           <span class="input-group-addon">
            <input class="isAnswer" type="checkbox" value="1" name="formData[options][{%=o.count%}][isAnswer]" aria-label="Checkbox for following text input">
          </span></td>
    </tr>
</script>
<script type="text/x-tmpl" id="factors">
    <label>Factors</label>
    {% if(o.factor == "fielding") { %}
    <select class="select2_single form-control select_player_condition" tabindex="-1" name="formData[conditions][player][{%=o.count-1%}][factor]">
    <?php foreach ($this->object['factors']['player']['fielding'] as $key => $value) { ?>
        <option value="_<?php echo $key; ?>_"><?php echo $value; ?></option>
    <?php } ?>
    </select>
    {% } else if(o.factor == "bowler") { %}
    <select class="select2_single form-control select_player_condition" tabindex="-1" name="formData[conditions][player][{%=o.count-1%}][factor]">
    <?php foreach ($this->object['factors']['player']['bowler'] as $key => $value) { ?>
        <option value="_<?php echo $key; ?>_"><?php echo $value; ?></option>
    <?php } ?>
    </select>
    {% } else { %}
    <select class="select2_single form-control select_player_condition" tabindex="-1" name="formData[conditions][player][{%=o.count-1%}][factor]">
    <?php foreach ($this->object['factors']['player']['batsmen'] as $key => $value) { ?>
        <option value="_<?php echo $key; ?>_"><?php echo $value; ?></option>
    <?php } ?>
    </select>
    {% } %}
</script>
