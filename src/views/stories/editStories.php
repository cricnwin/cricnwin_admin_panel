<?php 
$selectedCategories = array();
foreach ($this->object['stories']['categories'] as $key => $value) {
    # code...
    $selectedCategories[] = $value['categoryId'];
} ?>
<div class="row tile_count">
    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
        <div class="count">Add/Edit Story</div>
    </div>
    
</div>


<form action="" method="POST" id="add_story">
    <div class="clearfix"></div>
    <div class="row">
        <div class="form-group">
            <div data-parsley-validate="" class="form-horizontal" novalidate="">
                <div class="form-group col-lg-1 col-md-1 col-sm-1 col-xs-2">
                    <center><label class="control-label" for="first-name">Title:</label></center>
                </div>
                <div class="col-md-11 col-sm-10 col-xs-10  top_search">
                    <div class="input-group" style="width: 99%;">
                        <input type="text" name="newsTitle" class="form-control" placeholder="Type Headline..." value="<?php echo $this->object['stories']['newsTitle']; ?>">
                    </div>
                </div>
                
                <?php if(empty($_REQUEST['story-key']) == false) { ?>
                	<input type="hidden" name="newsId" value="<?php echo $_REQUEST['story-key']; ?>"/>
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="row margin_bottom_20">
                    <div class="form-inline">
                        <div class="form-group col-lg-8 col-md-8 col-sm-8 col-xs-12">
                            <label>Type:</label>
                            <div id="hint_box" style="display:inline;width: 65%;">
                                <input name="thumbImgUrl" class="form-control" style="width: 65%;" type="file" placeholder="Text/Image" value="<?php echo $this->object['stories']['thumbImgUrl']; ?>">
<!--                                <input type="text" class="form-control" style="display:none;" name="thumbImgUrl" value=""/>-->
                            </div>
                        </div>
                        <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-4">
                            <label class="padding_top_5 pull-left">Tag Category:</label>
                            </div>
                            <div class="form-group col-lg-8 col-md-8 col-sm-8 col-xs-10">
                                <select id="categoryMultiSelect" style="width:100%;" name="categories[][categoryId]" class="form-control" multiple="multiple">
                                <?php foreach ($this->object['category'] as $key => $category) { ?>
                                    <option value="<?php echo $category['categoryId']; ?>" <?php if(in_array($category['categoryId'], $selectedCategories) == true){ echo "selected"; } ?>><?php echo $category['catName']; ?></option>
                                <?php } ?>
                                </select>
                            </div>
                        </div>
                        
                    </div>
                </div>
                <div class="clearfix"></div>
                <br/>
                <div class="row margin_bottom_30">
                    <div class="form-inline">
                        <div class="row margin_bottom_30">
                            <div class="form-inline">
                                <div class="form-group col-lg-2 col-md-4 col-sm-4 col-xs-2">
                                    <label class="padding_top_5 pull-left margin_right_5">ThumbImgUrl</label>
                                </div>
                                <div class="form-group col-lg-10 col-md-8 col-sm-8 col-xs-10">
                                    <input  disabled=disabled style="width: 95%;" type="text" class="thumbImgUrl form-control pull-left" value="<?php echo $this->object['stories']['thumbImgUrl']; ?>"/>
                                </div>
                            </div>
                        </div>

                        <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-4">
                            <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-2">
                            <label class="padding_top_5 pull-left margin_right_5">Coins:</label>
                            </div>
                            <div class="form-group col-lg-8 col-md-8 col-sm-8 col-xs-10">
                                <input name="newsCoins" type="text" class="form-control pull-left" value="<?php if(empty($this->object['stories']['newsCoins']) == false){ echo $this->object['stories']['newsCoins']; } else { echo "0"; } ?>"/>
                            </div>
                        </div>
                        <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-4">
                            <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-2">
                            <label class="padding_top_5 pull-left margin_right_5">Runs:</label>
                            </div>
                            <div class="form-group col-lg-8 col-md-8 col-sm-8 col-xs-10">
                                <input name="newsRuns" type="text" class="form-control pull-left" value="<?php echo $this->object['stories']['newsRuns']; ?>"/>
                            </div>
                        </div>
                        <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-4">
                            <div class="form-group col-lg-2 col-md-2 col-sm-2 col-xs-2">
                            <label class="padding_top_5 pull-left">Source:</label>
                            </div>
                            <div class="form-group col-lg-8 col-md-8 col-sm-8 col-xs-10">
                                <select style="width:100%;" name="sourceId" class="form-control">
                                <?php foreach ($this->object['sources'] as $key => $source) { ?>
                                    <option value="<?php echo $source['sourceId']; ?>" <?php if($this->object['stories']['sourceId'] == $source['sourceId']) { echo "selected"; } ?>><?php echo $source['sourceName']; ?></option>
                                <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <br/>
                <div class="row margin_bottom_30">
                    <div class="form-inline">
                        <div class="form-group col-lg-2 col-md-4 col-sm-4 col-xs-2">
                        <label class="padding_top_5 pull-left margin_right_5">Short Description:</label>
                        </div>
                        <div class="form-group col-lg-10 col-md-8 col-sm-8 col-xs-10">
                            <input name="newsShortDescription" style="width: 95%;" type="text" class="form-control pull-left" value="<?php echo $this->object['stories']['newsShortDescription']; ?>"/>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <br/>
                <div class="row margin_bottom_30">
                    <div class="form-inline">
                        <div class="form-group col-lg-2 col-md-4 col-sm-4 col-xs-2">
                        <label class="padding_top_5 pull-left margin_right_5">Long Description:</label>
                        </div>
                        <div class="form-group col-lg-10 col-md-8 col-sm-8 col-xs-10">
                            <input type="hidden" class="form-control" name = "newsDetails[0][newsType]" value = "text"/>
                            <textarea name="newsDetails[0][newsValue]" style="width: 95%;min-height:250px;" class="form-control"><?php echo $this->object['stories']['newsDetails'][0]['newsValue']; ?></textarea>
                            <input type="hidden" name="newsDetails[0][newsPriority]" value="<?php if(empty($this->object['stories']) == false) {echo $this->object['stories']['newsDetails'][0]['newsPriority'];}else {echo '0'; } ?>">
                            <input type="hidden" class="form-control" name = "newsDetails[1][newsType]" value = "thumb_image"/>
                            <input type="hidden" class="form-control" name = "newsDetails[1][newsPriority]" value = "0"/>
                            <input type="hidden" class="thumbImgUrl form-control" name = "newsDetails[1][newsValue]" value="<?php echo $this->object['stories']['thumbImgUrl']; ?>"/>

                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <button type="submit" class="btn btn-success" id="sumbitForm">Submit</button>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</form>

