<?php
?>
<!-- Match List Starts -->
<div class="row top_tiles" style="margin: 10px 0;">
  <div class="col-md-6 col-sm-6 col-xs-12 tile">
     <h2>Edit Stories</h2>
  </div>
</div>
<div class="row" style="margin: 10px 0;">
  <div class="col-md-4 col-sm-4 col-xs-8 tile pull-left">
    <div class="input-group">
  		<span class="input-group-addon">Default Coins:&nbsp;</span>
  		<input type="text" class="form-control"/>
  	</div>
  </div>
  <div class="col-md-4 col-sm-4 col-xs-4 tile pull-right">
     <a href="edit-story" class="btn btn-primary">
     	<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>&nbsp;&nbsp;Add Story
     </a>
  </div>
</div>
<!-- Match List End -->

<div class="row">
<div class="col-md-12 col-sm-12 col-xs-12">
 <div class="x_panel">
    <div class="table-responsive">
       <div class="x_title">
          <h4 class="margin_0">Stories:</h4>
          <div class="clearfix"></div>
       </div>
       <table id="live_table" class="table table-bordered">
          <thead>
             <tr>
                <th width="5%">Order</th>
                <th>S. No.</th>
                <th>Story Id</th>
                <th>Title</th>
                <th>Type</th>
                <th>Time</th>
                <th>Tag Category</th>
                <th>Coins</th>
                <th>Status</th>
                <th>Action</th>
             </tr>
          </thead>
          <tbody>
          	<?php foreach ($this->object['stories'] as $key => $storiesDetails) { 
          		
          		 $icon = '<span class="glyphicon glyphicon-picture" aria-hidden="true"></span>';
          		 if($storiesDetails['sourceIcon'] == "videoicon") {
          		 	$icon = '<span class="glyphicon glyphicon-film" aria-hidden="true"></span>';
          		 }
          		 $time = strtotime($storiesDetails['newsCreateDate']);
          	?>
             <tr>
                <td class="text-center"><img class="drag-icon" src="public/img/drag-small.png"/></td>
                <td><?php echo $key+1; ?></td>
                 <td><?php echo $storiesDetails['newsId']; ?></td>
                 <td><?php echo $storiesDetails['newsTitle']; ?></td>
                <td><?php echo $icon; ?></td>
                <td><?php echo date("Y:m:d H:m:s", $time); ?></td>
                <td>
					<select>
					<?php foreach ($storiesDetails['categories'] as $key => $category) { ?>
						<option><?php echo $category['categoryName']; ?></option>
					<?php } ?>
					</select>	
                </td>
                <td><?php echo $storiesDetails['newsCoins']; ?></td>
                <td><?php echo $storiesDetails['newsStatus']; ?></td>
                <td>
                	<form action="edit-story" method="POST">
	                 <button type="submit" name="edit-story" class="btn btn-primary btn-xs">Edit</button>
	                <input type="hidden" name="story-key" value="<?php echo $storiesDetails['newsId']; ?>"/>
	              </form>	
                	<button type="button" class="btn btn-primary btn-xs">Remove</button>
                </td>
             </tr>
  			<?php } ?>
          </tbody>
       </table>
    </div>
 </div>
</div>
</div>
<div class="row">
	<div class="col-md-6 col-sm-6 col-xs-6">
		<div class="x_panel">
			<div class="table-responsive">
				<div class="x_title">
          <div class="row">
            <div class="col-md-8 col-sm-8 col-xs-8 tile pull-left">
               <h4 class="margin_0">Categories:</h4>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-4 tile pull-right">
               <a href="#" class="btn btn-primary add_button" data-toggle="modal" data-target=".add_image" class="btn btn-primary accordionIcon" data-type="category">
                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>&nbsp;&nbsp;Add Category
               </a>
            </div>
          </div>
  				<div class="clearfix"></div>
				</div>
        
				<table id="live_table" class="table table-bordered">
				<thead>
				 <tr>
				    <th>S.No.</th>
				    <th>Category</th>	
				    <th>Icon</th>
            <th>Action</th>
				 </tr>
				</thead>
				<tbody>
				<?php foreach ($this->object['category'] as $key => $categoryDetails) { 
	          		
	          	?>
             <tr>
             	  <td><?php echo $key+1; ?></td>
                <td class="text-center">
                	<?php echo $categoryDetails['catName']; ?>
                </td>
                <td><?php echo $categoryDetails['icon']; ?></td>
                <td>
                  <a href="#" data-toggle="modal" data-target=".add_image" class="btn btn-primary btn-xs accordionIcon add_button" data-type="category" data-cat-id="<?php echo $categoryDetails['categoryId']; ?>" data-cat-name="<?php echo $categoryDetails['catName']; ?>" data-icon="<?php echo $categoryDetails['icon']; ?>">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>&nbsp;&nbsp;Edit
                   </a>
                </td>
          		<?php } ?>
				</tbody>
				</table>
			</div>
		</div>
	</div>
	<div class="col-md-6 col-sm-6 col-xs-6">
		<div class="x_panel">
			<div class="table-responsive">
        <div class="x_title">
          <div class="row">
            <div class="col-md-8 col-sm-8 col-xs-8 tile pull-left">
               <h4 class="margin_0">Sources:</h4>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-4 tile pull-right">
               <a href="#" class="btn btn-primary add_button" data-toggle="modal" data-target=".add_image" class="btn btn-primary accordionIcon" data-type="source">
                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>&nbsp;&nbsp;Add Sources
               </a>
            </div>
          </div>
          <div class="clearfix"></div>
        </div>
				<table id="live_table" class="table table-bordered">
				<thead>
				 <tr>
				    <th>S. No.</th>
				    <th>Source</th>	
				    <th>Url</th>
				    <th>Icon</th>
				    <th>Status</th>
            <th>Action</th>
				 </tr>
				</thead>
				<tbody>
				<?php foreach ($this->object['source'] as $key => $sourceDetails) { 
      					
	          		 $time = strtotime($storiesDetails['newsCreateDate']);
	          	?>
             <tr>
                <td><?php echo $key+1; ?></td>
                <td><?php echo $sourceDetails['sourceName']; ?></td>
                <td><?php echo $sourceDetails['sourceUrl']; ?></td>
                <td><?php echo $sourceDetails['sourceIcon']; ?></td>
                <td><?php echo $sourceDetails['status'] == 1 ? "Active":"Deactive"; ?></td>
                <td>
                  <a href="#" data-toggle="modal" data-target=".add_image" class="btn btn-primary btn-xs accordionIcon add_button" data-type="source" data-source-id="<?php echo $sourceDetails['sourceId']; ?>" data-source-name="<?php echo $sourceDetails['sourceName']; ?>" data-icon="<?php echo $sourceDetails['sourceIcon']; ?>" data-source-url="<?php echo $sourceDetails['sourceUrl']; ?>" data-status="<?php echo $sourceDetails['status']; ?>">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>&nbsp;&nbsp;Edit
                   </a>
                </td>
          		<?php } ?>
				</tbody>
				</table>
			</div>
		</div>
	</div>
</div>


<div class="modal fade add_image" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-lg">
<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
    </div>
    <div class="modal-body">
    
    </div>
</div>
</div>
</div>

<script type="text/x-tmpl" id="add_category">
  <form type="POST" id="addCategory">
      <table  class="table add_image_container table-bordered">
      <tbody>
          <tr>
              <td>
                <div class="form-inline">
                    <label>Category Name:</label>
                </div>          
              </td>
               <td>
                <div class="form-inline">
                  {% if(o.catId != "") { %}
                  <input class="form-control" type="hidden" name="catId" placeholder="" value="{%=o.catId%}">
                  {% } %}
                  <input class="form-control" style="width:100%;" name="catName" placeholder="" value="{%=o.catName%}" required="">
                </div>          
              </td>
           </tr>
           <tr>
              <td>
                <div class="form-inline">
                    <label>Icon:</label>
                </div>
              </td>
              <td>
                <div class="form-inline">
                    <input class="form-control" style="width:100%;"  name="icon" placeholder="" value="{%=o.icon%}" required="">
                </div>
              </td>
           </tr>
           <tr>
              <td>
                <div class="form-inline">
                    <label>Status:</label>
                </div>
              </td>
              <td>
                <div class="form-inline">
                    <input class="form-control" style="width:100%;"  name="status" placeholder="" value="{%=o.status%}" required="">
                </div>
              </td>
            </tr>
            <tr>
              <td class="text-center" colspan="2"><button type="submit" class="btn btn-primary">Save</button></td>

          </tr>
      </tbody>
  </table>
  </form>
</script>
<script type="text/x-tmpl" id="add_sources">
  <form type="POST" id="addSource">
      <table  class="table add_image_container table-bordered">
      <tbody>
          <tr>
              <td>
                <div class="form-inline">
                    <label>Source Name:</label>
                </div>          
              </td>
               <td>
                <div class="form-inline">
                  {% if(o.sourceId) { %}
                  <input class="form-control" type="hidden" name="sourceId" placeholder="" value="{%=o.sourceId%}">
                  {% } %}
                  <input class="form-control" style="width:100%;"  name="sourceName" placeholder="" value="{%=o.sourceName%}" required="">
                </div>          
              </td>
           </tr>
           <tr>
              <td>
                <div class="form-inline">
                    <label>Source URL:</label>
                </div>          
              </td>
               <td>
                <div class="form-inline">
                  <input class="form-control" style="width:100%;"  name="sourceUrl" placeholder="" value="{%=o.sourceUrl%}" required="">
                </div>          
              </td>
           </tr>
           <tr>
              <td>
                <div class="form-inline">
                    <label>Icon:</label>
                </div>
              </td>
              <td>
                <div class="form-inline">
                    <input class="form-control" style="width:100%;"  name="sourceIcon" placeholder="" value="{%=o.sourceIcon%}" required="">
                </div>
              </td>
           </tr>
           <tr>
              <td>
                <div class="form-inline">
                    <label>Status:</label>
                </div>
              </td>
              <td>
                <div class="form-inline">
                    <input class="form-control" style="width:100%;"  name="status" placeholder="" value="{%=o.status%}" required="">
                </div>
              </td>
            </tr>
            <tr>
              <td class="text-center" colspan="2"><button type="submit" class="btn btn-primary">Save</button></td>

          </tr>
      </tbody>
  </table>
  </form>
</script>