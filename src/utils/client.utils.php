<?php

class Client
{
	public function post($url, $param, $header = array())
	{
		$ch = curl_init();
		$curlConfig = array(
			CURLOPT_HTTPHEADER 	   => $header,
			CURLOPT_HEADER         => 0,
		    CURLOPT_URL            => $url,
		    CURLOPT_POST           => true,
		    CURLOPT_CUSTOMREQUEST  => "POST",
		    CURLOPT_RETURNTRANSFER => true,
		    CURLOPT_POSTFIELDS     => $param,
		    CURLOPT_HTTPHEADER 	   => array(
                "cache-control: no-cache",
                "content-type: application/json"
            )
		);
		curl_setopt_array($ch, $curlConfig);
		$result = curl_exec($ch);
		curl_close($ch);

		return $result;
	}

	public function get($url, $header = array())
	{
		$ch = curl_init();
	    $headers = array(
		    'Accept: application/json',
		    'Content-Type: application/json'
	    );
	    curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_HEADER, 0);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	    // Timeout in seconds
	    curl_setopt($ch, CURLOPT_TIMEOUT, 30);

	    $result = curl_exec($ch);
		curl_close($ch);

		return $result;
	}
}

?>