<?php 
ob_start();
session_start();

date_default_timezone_set('Asia/Kolkata');

include_once("vendor/autoload.php");
include_once("config/config.class.php");
include_once("config/logger.class.php");


$action = empty($_REQUEST['action']) == false ? $_REQUEST['action'] : "dashboard";
$controller = $action.".controller.php";

include_once("controllers/".$controller);

$class = ucwords($action);
$object = new $class();

?>
<!DOCTYPE html>
<html lang="en">
  <head>
  	<?php include_once("views/common/css_loader.php"); ?>
  </head>
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
      		<?php include_once("views/common/sideMenu.php"); ?>
      		<?php include_once("views/common/header.php"); ?>
      		<div class="right_col" role="main">
      			<?php $object->load(); ob_flush();?>
      		</div>
      </div>
  	</div>
  	<?php include_once("views/common/script_loader.php"); ?>
  </body>
</html>

