<?php
include_once("config/config.class.php");
include_once("utils/client.utils.php");


class User
{
	public function __construct()
	{
		$this->client = new Client();
		$this->views = $step = empty($_REQUEST['step']) == false ? $_REQUEST['step'] : "user";
		$action = $this->getActionByStep($step);
		$this->$action();
	}

	public function load()
	{
		include_once("views/user/".$this->views.".php");
	}

	public function getUserProfile()
	{

	}
 
	public function getActionByStep($step)
	{
		$action = "";
		switch ($step) {
			default:
				# code...
				$action = "getUserProfile";
				break;
		}
		return $action;
	}
}
?>