<?php
include_once("config/config.class.php");
include_once("utils/client.utils.php");

class Dashboard
{
	public function __construct()
	{
		$this->log = new LoggerClass();
		$this->client = new Client();

		$step = empty($_REQUEST['step']) == false ? $_REQUEST['step'] : "dashboard";
		$action = $this->getActionByStep($step);
		$this->view = $step;
		$this->$action();

		$this->log->info("My first msession_get_array(session)ge");
	}

	public function load()
	{
			
		if($this->view != "") {
			include_once("views/dashboard/".$this->view.".php");
		}
	}

	public function getActionByStep($step)
	{
		$action = "";
		switch ($step) {
			default: $action = "getDashboardData"; break;
		}
		return $action;
	}

	public function getDashboardData()
	{

	}
} 