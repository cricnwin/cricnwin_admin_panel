<?php
include_once("config/config.class.php");
include_once("utils/client.utils.php");


class Quiz
{
	public function __construct()
	{
		$this->log = new LoggerClass();
		$this->client = new Client();

		$step = empty($_REQUEST['step']) == false ? $_REQUEST['step'] : "matchList";
		$action = $this->getActionByStep($step);
		$this->view = $step;
		$this->$action();

		$this->log->info("My first mesage");
	}

	public function load()
	{
		if($this->view != "") {
			include_once("views/quiz/".$this->view.".php");
		}
	}

	public function getMatchList()
	{
		$url = Config::$apiEndPoint2."match/list/recent?days=12";
		$response = $this->client->get($url);
		$this->object = json_decode($response, true);

		foreach ($this->object['data'] as $key => $value) {
		    if ($value['isLive']) {
                $this->liveMatchList[] = $value;
            } else {
		        $matchEndDateTime = strtotime($value['matchEndDateIst']);

		        if ($matchEndDateTime < time()) {
                    $this->completedMatchList[] = $value;
                } else {
                    $this->upcomingMatchList[] = $value;
                }
             }
		}
	}

	public function getSchedule()
    {
//            if (isset($_REQUEST['month']) && empty($_REQUEST(['month']) == false)) {
//                $month = $_REQUEST(['month']);
//            }
//            else {
//                // f$timeZone = new DateTimeZone('Asia/Kolkata');
//                //$now = new DateTime("now", $timeZone );
//                //$month =  $now->format("Y-m");
//                //$month = date("Y-m", strtotime('-1 month'));
//                //$month = date("Y-m");
//                //$endMonth = date("Y-m", strtotime('+1 month'));
//            }
            $start = 0;
            $end = 50;
            $url = Config::$apiEndPoint2."match/list/" . $start ."/".$end;
        //$url = Config::$apiEndPoint."match/schedule/" . $month ;
        $response = $this->client->get($url);
            $this->object = json_decode($response, true);
            if($this->object['status'] == 500) {
                echo("no match present");
                exit;
            }
            else {
                foreach ($this->object['data'] as $key => $value) {
                    if ($value['isLive']) {
                        $this->liveMatchList[] = $value;
                    } else {
                        $matchEndDateTime = strtotime($value['matchEndDateIst']);

                        if ($matchEndDateTime < time()) {
                            $this->completedMatchList[] = $value;
                        } else {
                            $this->upcomingMatchList[] = $value;
                        }
                    }
                }
            }
    }

	public function getAddQuizParams()
	{
		if(empty($_REQUEST['match-key']) == false) {
			$key = $_REQUEST['match-key'];
			//TODO Change match key
			$url = Config::$apiEndPoint2 . "dashboard/play/".$key."/addQuiz";
			$response = $this->client->get($url);
			$this->object = json_decode($response, true)['data'];
            //print_r($this->object);
        } else {
			header("Location: match-list"); 
			exit();
		}
	}

	public function viewQuiz()
	{
		if(empty($_REQUEST['match-key']) == false) {
			$key = $_REQUEST['match-key'];
			$url = Config::$apiEndPoint."dashboard/play/".$key."/viewQuiz";
			$response = $this->client->get($url);
			$response = json_decode($response, true)['data'];

			foreach ($response as $key => $quizDetails) {
				if(empty($quizDetails) == false) {
					# code...
					if($quizDetails['questionStatus'] == "ACTIVE") {
						$this->object['questionsActive'] += 1;
					}
					$this->object['questionsTotal'] += 1;
					$this->object['quiz'][] = $quizDetails;
				}
			}
		} else {
			header("Location: match-list"); 
			exit();
		}
	}

    public function viewClaimList()
    {
        if(empty($_REQUEST['match-key']) == false) {
            $key = $_REQUEST['match-key'];
            $url = Config::$apiEndPoint."dashboard/play/".$key."/claim-reward";
            $response = $this->client->get($url);
            $response = json_decode($response, true)['data'];
            foreach ($response as $key => $claimDetails) {
                if(empty($claimDetails) == false) {
                    # code...
                    $this->claimList[] = $claimDetails;
                }
            }
        } else {
            header("Location: match-list");
            exit();
        }
    }


	public function addQuiz()
	{
		$formParam = $_REQUEST['formData'];
		$matchKey = $_REQUEST['match-key'];

		$inputParam = array();
		$inputParam['question'] = $formParam['question'];
		foreach ($formParam['options'] as $key => $value) {
			# code...
			if(!empty($value['text']) && !empty($value['probability'])) {
				$inputParam['options'][]  = array(
					"text" => $value["text"],
					"probability" => $value["probability"],
					"isAnswer" => $value["isAnswer"] == "on" ? 1 : 0
				);
			}
		}

		if (count($formParam['conditions']['team']) > 0) {
            foreach ($formParam['conditions']['team'] as $index => $listOfParam) {
                $limit = "";
                if($listOfParam['condition'] == "><") {
                    $minValue = empty($listOfParam['minValue']) ? 0 : $listOfParam['minValue'];
                    $maxValue = empty($listOfParam['maxValue']) ? 0 : $listOfParam['maxValue'];
                    $limit 	  = $listOfParam['factor']." >= ".$minValue." && ".$listOfParam['factor']." <= ".$maxValue;
                } else if($listOfParam['condition'] == ">") {
                    $value = empty($listOfParam['value']) ? 0 : $listOfParam['value'];
                    $limit 	  = $listOfParam['factor']." >= ".$value;
                } else if($listOfParam['condition'] == "<") {
                    $value = empty($listOfParam['value']) ? 0 : $listOfParam['value'];
                    $limit 	  = $listOfParam['factor']." <= ".$value;
                }
                $listOfParam['innings'] = empty($listOfParam['innings']) ? 1 : $listOfParam['innings'];
                $listOfParam['team'] = empty($listOfParam['team']) ? 'a' : $listOfParam['team'];
                $inputParam['conditions']['team'][] = array(
                    "innings" => $listOfParam['team']."_".$listOfParam['innings'],
                    "limit" => $limit
                );
            }
        }

		if (count($formParam['conditions']['player']) > 0) {
            foreach ($formParam['conditions']['player'] as $index => $listOfParam) {
                $limit = "";

                if ($listOfParam['factor'] == "_dismissed_") {
                    $limit = "_dismissed_ == true";
                }

                if($listOfParam['condition'] == "><") {
                    $minValue = empty($listOfParam['minValue']) ? 0 : $listOfParam['minValue'];
                    $maxValue = empty($listOfParam['maxValue']) ? 0 : $listOfParam['maxValue'];
                    $limit 	  = $listOfParam['factor']." >= ".$minValue." && ".$listOfParam['factor']." <= ".$maxValue;
                } else if($listOfParam['condition'] == ">") {
                    $value = empty($listOfParam['value']) ? 0 : $listOfParam['value'];
                    $limit 	  = $listOfParam['factor']." >= ".$value;
                } else if($listOfParam['condition'] == "<") {
                    $value = empty($listOfParam['value']) ? 0 : $listOfParam['value'];
                    $limit 	  = $listOfParam['factor']." <= ".$value;
                }
                $listOfParam['innings'] = empty($listOfParam['innings']) ? 1 : $listOfParam['innings'];
                $inputParam['conditions']['player'][] = array(
                    "id" => $listOfParam['key'],
                    "innings" => $listOfParam['team']."_".$listOfParam['innings'],
                    "acting_as" => $listOfParam['acting_as'],
                    "limit" => $limit
                );

            }
        }

		$url = Config::$apiEndPoint."dashboard/play/".$matchKey."/quiz";
		$response = $this->client->post($url, json_encode($inputParam));
		echo($response); exit;
	}

	public function getQuizInfo()
	{
		$matchKey = $_REQUEST['match-key'];
		$quizId = $_REQUEST['quizId'];
		if(empty($matchKey) == false && empty($quizId) == false) {
			$url = Config::$apiEndPoint."dashboard/play/".$matchKey."/".$quizId."/detail";
			$response = $this->client->get($url);
			$this->object = json_decode($response, true)['data'];
		}
		echo json_encode($this->object); exit;
	}

	public function updateProbability()
	{
		$matchKey = $_REQUEST['match-key'];
		$quizId = $_REQUEST['quizId'];
		$optionId = $_REQUEST['optionId'];
		$probability = $_REQUEST['probability'];
		if(empty($matchKey) == false && empty($quizId) == false) {
			$params = array(
			  "questionId" => $quizId,
		      "optionId" =>	  $optionId,
		      "optionProbability" =>  $probability
			);

			$url = Config::$apiEndPoint."dashboard/play/".$matchKey."/question/probability";
			$response = $this->client->post($url, json_encode($params));
		}
		echo $response; exit;
	}

	public function updateAnswer()
	{
		$matchKey = $_REQUEST['match-key'];
		$quizId = $_REQUEST['quizId'];
		$optionId = $_REQUEST['optionId'];
		if(empty($matchKey) == false && empty($quizId) == false) {
		
			$url = Config::$apiEndPoint."dashboard/play/".$matchKey."/".$quizId."/answer?optionId=".$optionId;
			$response = $this->client->post($url,'');
		}
		echo $response; exit;
	}

	public function disableQuiz()
	{
		$matchKey = $_REQUEST['match-key'];
		$quizId = $_REQUEST['quizId'];
		$status = $_REQUEST['status'];
		if(empty($matchKey) == false && empty($quizId) == false) {
		
			$params = array(
			  "questionId" => $quizId,
		      "status" 	   => $status
		    );
			$url = Config::$apiEndPoint."dashboard/play/".$matchKey."/question/status";
			$response = $this->client->post($url, json_encode($params));
		}
		echo $response; exit;
	}

    public function callOffQuiz()
    {
        $matchKey = $_REQUEST['match-key'];
        $quizId = $_REQUEST['quizId'];
        if(empty($matchKey) == false && empty($quizId) == false) {
            $url = Config::$apiEndPoint."play/calloff/".$matchKey."/".$quizId;
            $response = $this->client->post($url);
        }
        echo $response;
        exit;
    }

    public function callOffMatch()
    {
        $matchKey = $_REQUEST['match-key'];
        if(empty($matchKey) == false) {
            $url = Config::$apiEndPoint."play/calloff/".$matchKey;
            $response = $this->client->post($url);
        }
        echo $response;
        exit;
    }

	public function disableMatch()
	{
		$matchKey = $_REQUEST['match-key'];
		$status = $_REQUEST['status'];
		if(empty($matchKey) == false) {
		
			$params = array(
			  "status" 	   => $status
		    );
			$url = Config::$apiEndPoint."dashboard/play/".$matchKey."/question/status";
			$response = $this->client->post($url, json_encode($params));
		}
		echo $response; exit;
	}

	public function getActionByStep($step)
	{
		$action = "";
		switch ($step) {
			case 'addQuiz': $action = "getAddQuizParams"; break;
			case 'uploadImage': $action = "uploadImage"; break;

			case 'saveQuiz': $action = "addQuiz"; break;
			case 'quizInfo': $action = "getQuizInfo"; break;
			case 'viewQuiz': $action = "viewQuiz"; break;
            case 'viewClaimList': $action = "viewClaimList"; break;
			case 'updateProbability': $action = "updateProbability"; break;
			case 'updateAnswer': $action = "updateAnswer"; break;
			case 'disableQuiz': $action = "disableQuiz"; break;
            case 'callOffQuiz': $action = "callOffQuiz"; break;
            case 'callOffMatch': $action = "callOffMatch"; break;
            case 'schedule' : $action = "getSchedule"; break;
			default: $action = "getMatchList"; break;
		}
		return $action;
	}
} 
