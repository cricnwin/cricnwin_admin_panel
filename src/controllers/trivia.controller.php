<?php
include_once("config/config.class.php");
include_once("utils/client.utils.php");


class Trivia
{
	public function __construct()
	{
		$this->client = new Client();
		$this->views = $step = empty($_REQUEST['step']) == false ? $_REQUEST['step'] : "trivia";
		$action = $this->getActionByStep($step);
		$this->$action();
	}

	public function load()
	{
		include_once("views/trivia/".$this->views.".php");
	}

	public function getListOfTrivia()
	{

	}
 
	public function getActionByStep($step)
	{
		$action = "";
		switch ($step) {
			default:
				# code...
				$action = "getListOfTrivia";
				break;
		}
		return $action;
	}
}
?>