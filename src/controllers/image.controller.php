<?php
include_once("config/config.class.php");
include_once("utils/client.utils.php");


class Image
{
	public function __construct()
	{
		$this->log = new LoggerClass();
		$this->client = new Client();

		$step = empty($_REQUEST['step']) == false ? $_REQUEST['step'] : "uploadImage";
		$action = $this->getActionByStep($step);
		$this->view = $step;
		$this->$action();

		$this->log->info("My first mesage");
	}

	public function load()
	{
		if($this->view != "") {
			include_once("views/uploadImages/".$this->view.".php");
		}
	}

	public function getListOfCarouselImages()
	{
		$url = Config::$apiEndPoint."image/carousel";
		$response = $this->client->get($url);
		$this->object = json_decode($response, true);
		$this->view = "carouselImage";
	}

	public function getMatchList()
	{
		$url = Config::$apiEndPoint."match/list/recent";
		$response = $this->client->get($url);
		$this->object = json_decode($response, true);
		
		foreach ($this->object['data'] as $key => $value) {
			if($value['status'] == "completed") {
				$this->completedMatchList[] = $value;
			} else if($value['status'] == "started") {
				$this->liveMatchList[] = $value;
			} else if($value['status'] == "notstarted") {
				$this->upcomingMatchList[] = $value;
			} 
		}
	}

	public function getActionByStep($step)
	{
		$action = "";
		switch ($step) {
			case 'carousel':
			$action = "getListOfCarouselImages"; break;
			case 'image':
			default: $action = "getMatchList"; break;
		}
		return $action;
	}
} 