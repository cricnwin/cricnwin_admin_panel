<?php
include_once("config/config.class.php");
include_once("utils/client.utils.php");


class Stories
{
	public function __construct()
	{
		$this->client = new Client();
		$this->views = $step = empty($_REQUEST['step']) == false ? $_REQUEST['step'] : "dashboard";
		$action = $this->getActionByStep($step);
		$this->$action();
	}

	public function load()
	{
		include_once("views/stories/".$this->views.".php");
	}
 
	public function getActionByStep($step)
	{
		$action = "";
		switch ($step) {
			case 'editStories':
				$action = "getStoriesDetails";
				break;
			case 'addStory':
				$action = "addStory";
				break;
			case 'addSource':
				$action = "addSources";
				break;
			case 'addCategory':
				$action = "addCategory";
				break;
			default:
				# code...
				$action = "getListOfStories";
				break;
		}
		return $action;
	}

	public function addStory()
	{
		$formParam = $_POST;
		$categories = $formParam['categories'];

		$formParam['status'] = 1;
		$formParam['categories'] = array();
		foreach ($categories as $key => $value) {
			# code...
			if(is_array($value['categoryId'])) {
				$formParam['categories'][]['categoryId'] = reset($value['categoryId']);
			} else{
				$formParam['categories'][]['categoryId'] = $value['categoryId'];
			}
		}
		$url = Config::$apiEndPoint."dashboard/story";
		$response = $this->client->post($url, json_encode($formParam));
		echo($response); exit;
	}

	public function addSources()
	{
		$formParam = $_POST;
		
		$url = Config::$apiEndPoint."dashboard/story/source";
		if(empty($formParam['sourceId']) == true) {
			unset($formParam['sourceId']);
		}
		echo json_encode($formParam);
		$response = $this->client->post($url, json_encode($formParam));
		echo($response); exit;
	}

	public function addCategory()
	{
		$formParam = $_POST;
		
		$url = Config::$apiEndPoint."dashboard/story/category";
		if(empty($formParam['catId']) == true) {
			unset($formParam['catId']);
		}
		$response = $this->client->post($url, json_encode($formParam));
		echo($response); exit;
	}

	public function getStoriesDetails()
	{
		$this->object['category'] = $this->getListOfCategories();
		$this->object['sources'] = $this->getListOfSources();
		if(empty($_REQUEST['story-key']) == false) {
			$storyId = $_REQUEST['story-key'];
			$url = Config::$apiEndPoint."dashboard/story/detail/".$storyId;
			$response = $this->client->get($url);
			$this->object['stories'] = json_decode($response, true)['data'];
		}
	}

	public function getListOfCategories()
	{
		$url = Config::$apiEndPoint."dashboard/story/category";
		$response = $this->client->get($url);
		return json_decode($response, true)['data'];
	}

	public function getListOfSources()
	{
		$url = Config::$apiEndPoint."dashboard/story/source";
		$response = $this->client->get($url);
		return json_decode($response, true)['data'];
	}

	public function getListOfStories()
	{
		$url = Config::$apiEndPoint."dashboard/story/list";
		$response = $this->client->get($url);
		$this->object['stories'] = json_decode($response, true)['data'];

		$this->object['category'] = $this->getListOfCategories();
		$this->object['source'] = $this->getListOfSources();
	}
}
?>