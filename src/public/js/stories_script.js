$(document).ready(function(){
	$("#live_table").DataTable( {
        "paging": true
    } );

    $('#categoryMultiSelect').multiselect({
        includeSelectAllOption: true, // add select all option as usual
        optionClass: function(element) {
            var value = $(element).val();

            if (value%2 == 0) {
                return 'odd'; // reversed
            }
            else {
                return 'even'; // reversed
            }
        }
    });


    $(document).on("change", "#newsType", function(){
        var val = $(this).val();
        if(val == "2") {
            $("#hint_box input[type='file']").hide();
            $("#hint_box input[type='text']").show();
        } else {
            $("#hint_box input[type='file']").show();
            $("#hint_box input[type='text']").hide();
        }
    });

    formdata = new FormData();      
    $("#hint_box input[type='file']").on("change", function() {
        var file = this.files[0];
        if (formdata) {
            var newsId = $("[name='newsId']").val();
            formdata.append("imageToUpload", file);
            $.ajax({
                url: apiEndPoint+"image/news",
                type: "POST",
                data: formdata,
                processData: false,
                contentType: false,
                success:function(response){
                    if(response.status == 200) {
                        var imageUri = response.data.imageUri;
                        $(".thumbImgUrl").val(imageUri);
                    }
                }
            });
        }                       
    }); 

    $("#add_story").submit(function(event){

        event.preventDefault();
        var formdata = $("#add_story").serializeObject()

        formdata['categories'] = []
        $("#categoryMultiSelect > option:selected").each(function(i) {
             formdata['categories'][i] = [];
             formdata['categories'][i]['categoryId'] = this.value[0];
        });
        $.ajax({
            url: "index.php?action=stories&step=addStory",
            type: "POST",
            data: formdata,
            success:function(response){
                notify("Successfully Updated!!")
                //window.location="stories"
            }
        });
    });

    $(document).on("click", ".add_button", function(event){
        var type = $(this).data("type")
        var html = ""
        if(type == "category") {
            var catId = $(this).data("catId")
            var catName = $(this).data("catName")
            var icon = $(this).data("icon")
            var status = $(this).data("status")
            if(status == "" || status == undefined) {
                status = 1;
            }
            html = tmpl("add_category", {"catId": catId, "catName": catName, "icon":icon, "status":status})
        } else {
            var sourceId = $(this).data("sourceId")
            var sourceName = $(this).data("sourceName")
            var sourceUrl = $(this).data("sourceUrl")
            var icon = $(this).data("icon")
            var status = $(this).data("status")
            if(status == "" || status == undefined) {
                status = 1;
            }
            html = tmpl("add_sources", {"sourceUrl": sourceUrl, "sourceName": sourceName, "sourceId" : sourceId, "icon":icon, "status":status})
        }
    
        $(".add_image .modal-body").html(html);
    });

    $(document).on("submit", "#addSource", function(event){
        event.preventDefault();
        $.ajax({
            url: "index.php?action=stories&step=addSource",
            type: "POST",
            data: $(this).serializeObject(),
            success:function(response){
                notify("Successfully Updated!!")
                $(".add_image .close").trigger("click")
                window.location="stories"
            }
        });
    })

    $(document).on("submit", "#addCategory", function(event){
        event.preventDefault();
        console.log($(this).serializeObject())
        $.ajax({
            url: "index.php?action=stories&step=addCategory",
            type: "POST",
            data: $(this).serializeObject(),
            success:function(response){
                notify("Successfully Updated!!")
                $(".add_image .close").trigger("click")
                window.location="stories"
            }
        });
    })

    jQuery.extend(jQuery.validator.messages, {
        required: "This field is required.",
        catname: "category name cannot be empty",
    });
});