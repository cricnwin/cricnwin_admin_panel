$(document).ready(function(){
	$('.accordionIcon').on('shown.bs.collapse', function () {
        $(this).find("i.indicator").removeClass("fa-plus-circle").addClass("fa-minus-circle");
    });

    $('.accordionIcon').on('hidden.bs.collapse', function () {
        $(this).find("i.indicator").removeClass("fa-minus-circle").addClass("fa-plus-circle");
    });

    $(document).on("click", ".expandMatch", function(){
        var matchkey = $(this).data("matchkey")
        var teamA = $(this).data("teamA")
        var teamB = $(this).data("teamB")

        var html = tmpl("option_box", {"matchKey": matchkey, "team_a": teamA, "team_b": teamB})
        $("#hide_"+matchkey).html(html);
    });

    $(document).on("submit", "#imageUpload", function(event){
        event.preventDefault();

        var data = $(this).serializeObject();
        var fileInput = $(this).find("input[type='file']")
        var file = $(fileInput)[0].files;

        if(data.key == "") {
            notify("Key cannot be empty!", "error")
            return false
        }
        if(file == undefined) {
            notify("Please select image file!", "error")
            return false
        }

        data["imageToUpload"] = file[0]
        var formdata = new FormData();      
        if (formdata) {
            formdata.append("imageToUpload", file[0]);
            $.ajax({
                url: apiEndPoint+"image?key="+data.key+"&type="+data.type,
                type: "POST",
                data: formdata,
                processData: false,
                contentType: false,
                success:function(response){
                	if(response) {
                		if(response.status == 200) {
	                	    notify("Updated Successfully")
                            $(".add_image .close").trigger("click")
	                    }
                	}
                }
            });
        } 
    });


    $(document).on("submit", "#carouselUpload", function(event){
        event.preventDefault();

        var data = $(this).serializeObject();
        var fileInput = $(this).find("input[type='file']")
        var file = $(fileInput)[0].files;

        if(data.landingUrl == "") {
            notify("landing page url cannot be empty!", "error")
            return false
        }
        if(data.displayOrder == "") {
            notify("display order cannot be empty!", "error")
            return false
        }
        if(file == undefined) {
            notify("Please select image file!", "error")
            return false
        }

        data["imageToUpload"] = file[0]
        var formdata = new FormData();      
        if (formdata) {
            formdata.append("imageToUpload", file[0]);
            formdata.append("landingUrl", data.landingUrl);
            formdata.append("displayOrder", data.displayOrder);
            $.ajax({
                url: apiEndPoint+"image/carousel",
                type: "POST",
                data: formdata,
                processData: false,
                contentType: false,
                success:function(response){
                    if(response) {
                        if(response.status == 200) {
                            notify("Updated Successfully")
                            $(".add_image .close").trigger("click")
                            window.location="carousel-image"
                        }
                    }
                }
            });
        } 
    });

    $(document).on("click", ".remove-carousel", function(event) {
        event.preventDefault();
        var carouselId = $(this).val();
            $.ajax({
                url: apiEndPoint+"carousel/delete/" + carouselId,
                type: "POST",
                processData: false,
                contentType: false,
                success:function(response) {
                    if(response) {
                        if(response.status == 200) {
                            notify("Deleted Successfully");
                            window.location="carousel-image"
                        }
                    }
                }
            });
        });
    
});