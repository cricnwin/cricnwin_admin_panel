

$(document).ready(function(){

    $('#clockpicker').datetimepicker();

    $('#monthpicker').datetimepicker({
        format: 'YYYY-MM'
    });


    if($("#add_quiz").length > 0) {
    	$("[name='condition_container']").html("<div name='condition_row'>"+tmpl("team_box", {"count": 1})+"</div>");

    	var addbutton = document.getElementById("addbutton");
	    addbutton.addEventListener("click", function() {
	        var count = $("[name=\"condition_box\"]").length + 1;
	        var copy_container = "<div name='condition_row'>"+tmpl("team_box", {"count": count})+"</div>";
	        $("[name='condition_container']").append(copy_container);
	    });
    }

    $('#monthpicker').on('dp.change', function() {
        var month = $(this).find("#month").val();
        console.log(month);
        if(month != "") {
            var data = {"month": month};
            $("body").loading({"stoppable":true});
            $.ajax({
                url: "index.php?action=quiz&step=schedule",
                type: "POST",
                data: data
            });
        }

    });

    $(document).on("change", ".condition_select_box", function(){
        var val = $(this).val();
        console.log(val)
        var parent = $(this).parents("[name='condition_box']")
        var count = $(parent).find(".count").val();
        var type = $(parent).find(".team_select").val()
        if(val == "><") {
            parent.find("[name='copy_container']").html(tmpl("param_box", {type:type, count: count, lies_btw:true}))
        } else if(val == "<") {
            parent.find("[name='copy_container']").html(tmpl("param_box", {type:type, count: count, less_than:true}))
        } else if(val == ">") {
            parent.find("[name='copy_container']").html(tmpl("param_box", {type:type, count: count, grt_than:true}))
        }
    });

    $(document).on("change", "#hintType", function(){
        var val = $(this).val();
        if(val == "image") {
        	$("#hint_box input[type='file']").show();
        	$("#hint_box textarea").hide();
        } else {
        	$("#hint_box input[type='file']").hide();
        	$("#hint_box textarea").show();
        }
    });

    $(document).on("change", "#nOfOption", function(){
        var val = $(this).val();
        var count = $("#optionTable tbody tr").length
        var html = "";
        if(val >= count) {
            for(var i = count; i < val; i++) {
            	html += tmpl("option_box", {count: i})
            }
            $("#optionTable tbody").append(html)
        } else {
            for(var i = 0; i < val; i++) {
                html += tmpl("option_box", {count: i})
            }
            $("#optionTable tbody").html(html)
        }
        
    }); 

    $(document).on("change", "[name='team_select']", function(){
        var val = $(this).val();
        var parent_div = $(this).parents("[name='condition_row']")[0]

        console.log(val)
        if(val == "team") {
            $(parent_div).html(tmpl("team_box", {"count": $("[name=\"condition_box\"]").length}));
        } else {
            $(parent_div).html(tmpl("player_box", {"count": $("[name=\"condition_box\"]").length}));
        }
    });

    $(document).on("change", ".select_factor", function(){
        var val = $(this).val();
        var parent_div = $(this).parents("[name='condition_box']")[0]
        var count = $(parent_div).find(".count").val();
        $(parent_div).find(".factor_box").html(tmpl("factors", {"factor": val, count: count}));
    });


    $(document).on("change", ".select_player_condition", function(){
        var val = $(this).val();
        val.indexOf("dismissed");
        console.log(val);
        if(val.indexOf("dismissed") != -1) {
            var parent_div = $(this).parents("[name='condition_box']")[0];
            var selectBox = $(parent_div).find(".condition_select_box");
            var copy_container = $(parent_div).find("[name='copy_container']");
            $(copy_container).remove();
            $(selectBox).remove();
        } else {
            var parent_div = $(this).parents("[name='condition_box']")[0];
            var selectBox = $(parent_div).find(".condition_select_box");
            var copy_container = $(parent_div).find("[name='copy_container']");
            $(copy_container).show();
            $(selectBox).show();
        }
    });

    $(document).on("click", ".isAnswer", function(){
        $(this).attr("checked", "checked")
    }); 

    $(document).on("click", ".expandQuiz", function(){
        var quizId = $(this).data("quizid")
        var matchkey = $(this).data("matchkey")
        $(this).find("i").collapse('toggle')
        if(quizId != "" && matchkey != "") {
        	if($("#hide_"+quizId).html() == "") {
	        	var element = this;
	        	var data = {"quizId": quizId, "match-key": matchkey}
	        	$("body").loading({"stoppable":true});
	        	$.ajax({
	                url: "index.php?action=quiz&step=quizInfo",
	                type: "POST",
	                context: {"quizId": quizId, "element": element},
	                data: data,
	                success:function(response){
	                	$("body").loading("stop");
	                	resJson = JSON.parse(response)
	                	if(resJson) {
	                		var html = tmpl("option_box", {"questionId": quizId, "questionDetails": resJson});
	                	    $("#hide_"+quizId).html(html);
	                	    $(element).find("i").click();
	                	    $(element).find("i").collapse('toggle')
	                    }
	                }
	            });
	        }
        }
    });

    $(document).on("click", ".probability", function(){
        var val = $(this).text();
        if(val != "") {
        	$(this).html('<input type="number" max="100" min="0" class="probabilityUpdate" style="width: 50px;" focus/>');
        }
    });

    $(document).on("keyup", ".probabilityUpdate", function(event){
    	if(event.keyCode == 13){
    		$("body").loading({
    			stoppable: false
    		});
    		var val = $(this).val();
    		var matchkey = $(this).parents("tr").find("[name='match-key']").val();
    		var quizId = $(this).parents("tr").find("[name='quizId']").val();
    		var optionId = $(this).parents("td").find("[name='optionId']").val();

    		$.ajax({
                url: "index.php",
                type: "POST",
                data: {
                	"action":"quiz", 
                	"step" :"updateProbability", 
                	"quizId": quizId, 
                	"match-key": matchkey, 
                	"optionId": optionId, 
                	"probability" : val
                },
                success:function(response){
                	$("body").loading("stop");
                	res = JSON.parse(response)
                	if(res.status=="200") {
                		notify("Probability updated!!")
                	} else {
                		notify("Something wrong happned!!", "error")
                	}
                }
            });
    		$(this).parent().html(val+' <i class="fa fa-pencil"></i>');
	    }
	});

    formdata = new FormData();      
    $("#hint_box input[type='file']").on("change", function() {
        var file = this.files[0];
        if (formdata) {

            var matchKey = $("[name='match-key']").val();
            formdata.append("imageToUpload", file);
            formdata.append("key", matchKey);

            $.ajax({
                url: apiEndPoint+"image?key="+matchKey+"&type=team",
                type: "POST",
                data: formdata,
                processData: false,
                contentType: false,
                success:function(response){
                    if(response.status == 200) {
                        $("[name='formData[question][hint]']").val(response.data.imageUri);
                    }
                }
            });
        }                       
    }) ; 

    $(".sumbitForm").click(function(event) {

        event.preventDefault();

        PNotify.removeAll();

        var key = $(this).data("key");
        var redirect = $(this).data("redirect");
    	var formdata = $("#add_quiz").serializeObject();
        var formClone = $("#add_quiz").serializeObject();

        console.log(formdata);

        if(formdata["formData[question][text]"] == undefined || formdata["formData[question][text]"] == "") {
            notify("Question text cannot be empty", "warning");
            return false;
        }
        if(formdata["formData[options][0][probability]"] == undefined || formdata["formData[options][0][probability]"] == "") {
            notify("Probability cannot be empty", "warning");
            return false;
        }
        if(formdata["formData[options][0][text]"] == undefined || formdata["formData[options][0][text]"] == "") {
            notify("Option text cannot be empty", "warning");
            return false;
        }

        delete formClone["formData[question][hintType]"];
        delete formClone["formData[question][hint]"];


        var formValues = Object.values(formClone) ;

        for (var i = 0 ; i < formValues.length; i ++) {
            if (formValues[i] == undefined || formValues[i] == "" || formValues[i] == "Select Factor" || formValues[i] == "Condition") {
                notify("Some value in the form is empty. Please verify! ", "warning");
                return false;
            }
        }
    	$.ajax({
            url: "index.php?action=quiz&step=saveQuiz",
            type: "POST",
            data: formdata,
            context: key,
            success:function(response){
                if(response == '{"status":200}') {
                    notify("Quiz Added Successfully !");

                    if (redirect == "match") {
                        var url = 'view-quiz';
                        var form = $('<form action="' + url + '" method="post">' +
                            '<input type="text" name="match-key" value="' + key + '" />' +
                            '</form>');
                        $('body').append(form);
                        form.submit();
                    }
                    else {
                        var url = 'add-quiz';
                        var form = $('<form action="' + url + '" method="post">' +
                            '<input type="text" name="match-key" value="' + key + '" />' +
                            '</form>');
                        $('body').append(form);
                        form.submit();
                    }
                } else {
            	   notify("Something went wrong!", "error")
                }
                //window.location="match-list"
            }
        });
    });

    $(document).on("click", ".addAnswer", function(){
    	var tr = $(this).parents("tr");
        var status = $(tr).find(".quizStatus").text();
        console.log(status);

        if(status == "INACTIVE") {
            $(tr).find(".isAnswerLabel").toggle();
            $(tr).find(".isAnswerCheckBox").toggle();
            $(tr).find(".isAnswerCheckBox input[type='checkbox']").attr("checked", false)
        } else {
            notify("Question state is ACTIVE. Answers can be added only when state is INACTIVE.", "warning")
        }
    	
    });

    $(document).on("click", ".disableQuiz", function(){
        $("body").loading({
            stoppable: false
        });
        var matchkey = $(this).parents("tr").find("[name='match-key']").val();
        var quizId = $(this).parents("tr").find("[name='quizId']").val();
        var status = $(this).parents("tr").find(".quizStatus").text();
        var updateStatus = "INACTIVE";
        if(status == "INACTIVE") {
            updateStatus = "INACTIVE";
        }
        console.log($(this).data("status"));

        if($(this).data("status") =="DELETED") {
            updateStatus = "DELETED";
        }
        var element = this;

        $.ajax({
            url: "index.php",
            type: "POST",
            data: {
                "action": "quiz",
                "step": "disableQuiz",
                "match-key": matchkey,
                "quizId": quizId,
                "status": updateStatus
            },
            context: {
                "element": this,
                "status": updateStatus,
                "key": matchkey
            },
            success:function(response){
                $("body").loading("stop");

                if(response) {
                    notify("Quiz disabled!!")
                } else {
                    notify("Something wrong happned!!", "error")
                }

                var tr = $(element).parents("tr")
                $(tr).find(".quizStatus").html(updateStatus)
                if(updateStatus == "INACTIVE") {
                    $(element).html("Disable Quiz")
                } else if(updateStatus == "ACTIVE"){
                    $(element).html("Disable Quiz")
                }

                var url = 'view-quiz';
                var form = $('<form action="' + url + '" method="post">' +
                  '<input type="text" name="match-key" value="' + matchkey + '" />' +
                  '</form>');
                $('body').append(form);
                form.submit();
            }
        });
    });

    $(document).on("click", ".callOffQuiz", function(){
        $("body").loading({
            stoppable: false
        });
        var matchkey = $(this).parents("tr").find("[name='match-key']").val();
        var quizId = $(this).parents("tr").find("[name='quizId']").val();
        var updateStatus = "CALLEDOFF";

        var element = this;

        $.ajax({
            url: "index.php",
            type: "POST",
            data: {
                "action": "quiz",
                "step": "callOffQuiz",
                "match-key": matchkey,
                "quizId": quizId,
                "status": updateStatus
            },
            context: {
                "element": this,
                "status": updateStatus,
                "key": matchkey
            },
            success:function(response){
                $("body").loading("stop");
                if(response) {
                    notify("Quiz called off!!")
                } else {
                    notify("Something wrong happened!!", "error")
                }

                var tr = $(element).parents("tr");
                $(tr).find(".quizStatus").html(updateStatus);
                if(updateStatus == "INACTIVE") {
                    $(element).html("Enable Quiz")
                } else if(updateStatus == "ACTIVE"){
                    $(element).html("Disable Quiz")
                }

                var url = 'view-quiz';
                var form = $('<form action="' + url + '" method="post">' +
                    '<input type="text" name="match-key" value="' + matchkey + '" />' +
                    '</form>');
                $('body').append(form);
                form.submit();
            }
        });
    });

    $("form[name='match-call-off']").submit(function(event){
        event.preventDefault();
        $("body").loading({
            stoppable: false
        });
        var formData = $(this).serializeObject();
        $.ajax({
            url: "index.php",
            type: "POST",
            data: formData,
            success:function(response){
                notify("Successfully updated!!")
                //window.location="match-list"
            }
        });
        $("body").loading("stop");
    });

    $(document).on("click", "[name='isAnswerCheck']", function(){
    	$("body").loading({
			stoppable: false
		});
		var val = $(this).val();
		var matchkey = $(this).parents("tr").find("[name='match-key']").val();
		var quizId = $(this).parents("tr").find("[name='quizId']").val();
		var optionId = $(this).parents("td").find("[name='optionId']").val();

		var postData = {
        	"action":"quiz", 
        	"step" :"updateAnswer", 
        	"quizId": quizId, 
        	"match-key": matchkey, 
        	"optionId": optionId 
        };
		$.ajax({
            url: "index.php",
            type: "POST",
            data: postData,
            context: postData,
            success:function(response){
            	console.log(postData.quizId)
            	$("body").loading("stop");

            	res = JSON.parse(response)
            	if(res.status=="200") {
            		notify("Answer updated!!")
            	} else {
            		notify(res.msg, "error")
            	}
            }
        });
        var td = $('[name="optionId"][value="'+optionId+'"]').parents("td");
    	var tr = $('[name="quizId"][value="'+quizId+'"]').parents("tr");

    	$(tr).find(".isAnswerLabel").html("")
    	$(td).find(".isAnswerLabel").html("selected")
    	$(tr).find(".isAnswerLabel").toggle();
		$(tr).find(".isAnswerCheckBox").toggle();
        $("body").loading("stop");
    });

    $('.accordionIcon').on('shown.bs.collapse', function () {
        $(this).find("i.indicator").removeClass("fa-plus-circle").addClass("fa-minus-circle");
    });

    $('.accordionIcon').on('hidden.bs.collapse', function () {
        $(this).find("i.indicator").removeClass("fa-minus-circle").addClass("fa-plus-circle");
    });

    $(document).on("click", ".removeCond", function() {
        $(this).parents("[name='condition_row']").remove()
    })
            
});