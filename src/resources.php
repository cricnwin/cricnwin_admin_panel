https://drive.google.com/open?id=0B_1zYi00J5OqRTc2MDNuOEJJZFU

Hi Manisha,
As discussed, below mentioned are apis/format for play add quiz.

Get match lists: http://35.154.189.101/v1/match/list/recent
status key will have values as notstarted, started, completed for upcoming, ongoing and past matches respectively. 

Get call endpoint : http://35.154.189.101/v1/dashboard/play/:matchKey/addQuiz

Get call example : http://35.154.189.101/v1/dashboard/play/kplt20_2017_g17/addQuiz


Post Call : 
Below is a examples of how I will be expecting data for adding new quiz for a match.

question : {
      text : "How many runs will India score before virat kohli gets out ? ",
      type : "play",
      hint : "some random text or a image url",
      hintType : text/imageUrl
      create_date : time of creation
}
options : [{
      option_text : " 50 Runs",
      option_probability : 0.45,
      create_date : time of creation,
       is_answer : 1    
},
{
      option_text : " 50 Runs",
      option_probability : 0.45,
      create_date : time of creation,
      is_answer : 0   
},
{
      option_text : " 50 Runs",
      option_probability : 0.45,
      create_date : time of creation,
      is_answer : 0    
},
{
      option_text : " 50 Runs",
      option_probability : 0.45,
      create_date : time of creation ,
      is_answer : 0   
}
]
conditions : {
      team : [{
            key : "ind",
            innings : b_1,
            limit : ['_runs_ > 50', '_wickets_ > 3']
      }],      
      player : [{
            key : "v_kohli",
            innings : 1,
            acting_as : "batsman",
            limit : ['_dismissed_ = true', '_runs_ < 50'']
      }],
      match : [{
            key : "indaus01_2017_one-day_01",
            limit : ['_won_ = ind']
      }]
}

============ Forwarded message ============
From : Kanwer Singh <kanwer@cricnwin.com>
To : "nitesh7malhotra"<nitesh7.malhotra@gmail.com>
Date : Thu, 28 Sep 2017 19:19:22 +0530
Subject : Re: Regd : Play Screen APIs
============ Forwarded message ============

Hi Manisha, 
Below given is the api list :

Get Call Play screen
http://35.154.189.101/v1/dashboard/play/:matchKey/addQuiz
http://35.154.189.101/v1/dashboard/play/:matchKey/viewQuiz
http://35.154.189.101/v1/dashboard/play/:matchKey/:questionId/detail

Post call Play screen 
http://35.154.189.101/v1/dashboard/play/:matchKey/question/update/probability

FORMAT FOR POST CALL : 
{
      "questionId" :
      "optionId" :
      "optionProbability" : 
}

Get Call stories screen

http://35.154.189.101/v1/dashboard/story/:start/:limit
http://35.154.189.101/v1/dashboard/story/detail/:storyId
http://35.154.189.101/v1/dashboard/story/category
http://35.154.189.101/v1/dashboard/story/source

Post Calls And Format for stories screen :
1) Adding new Category : 

ENDPOINT : 
http://35.154.189.101/v1/dashboard/story/category

FORMAT FOR POST CALL :
{
      "catId" : Only when updating existing category. Else this key will not come.
      "catName" : categoryName,
      "icon" : Icon,
      "status" : 1/0
}

2) Adding new news source : 
ENDPOINT : 
http://35.154.189.101/v1/dashboard/story/source

FORMAT FOR POST CALL : 
{
      "sourceName" : 
      "sourceUrl" :
      "status" : 
      "icon" : 
      "sourceId" : Only when updating existing source. Else this key will not come.
}

3) Add new story :
ENDPOINT : 
http://35.154.189.101/v1/dashboard/story

FORMAT FOR POST CALL : 
{
      "newsId" : Only when updating existing news. Else this key will not come.
      "newsTitle":
      "newsCoins" :
      "newsRuns" : 
      "status" : 
      "sourceId" :
      "newsShortDescription"
      "newsDetails" : [
            {
                  "newsValue": TEXT
                  "newsType" : ENUM (image/text/thumb_image)
                  "newsPriority" : INT
            }
      ]
      "categories" : [
            {
                  "categoryId" : 
            }
      ]
}