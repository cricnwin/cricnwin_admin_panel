<?php

// Include and configure log4php
Logger::configure(__DIR__.'/logger.xml');
 
/**
 * This is a classic usage pattern: one logger object per class.
 */
class LoggerClass
{
    /** Holds the Logger. */
    private $log;
 
    /** Logger is instantiated in the constructor. */
    public function __construct()
    {
        // The __CLASS__ constant holds the class name, in our case "Foo".
        // Therefore this creates a logger named "Foo" (which we configured in the config file)
        $this->log = Logger::getLogger(__CLASS__);
    }

    public function info($message)
    {
        $this->log->info($message);
    }
}
?>